	private List<String> contexts;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    
	@Before
    public void setUp() {
        Parser.MAX_PAYLOAD = 100;
        Parser.MAX_TURNS = 100;
	    contexts = new ArrayList<>();
        contexts.add("7 5 2 25 500\n" + "3\n" + "100 5 450\n" + "1\n" + "0 0\n" + "5 1 1\n" + "1\n" + "1 1\n" + "2\n" + "2 0");
        contexts.add("7 7 2 50 500\n" + "3\n" + "100 5 450\n" + "2\n" + "0 0\n" + "5 1 1\n" + "5 5\n" + "0 10 2\n" +
                "3\n" + "1 1\n" + "2\n" + "2 0\n" + "3 3\n" + "1\n" + "0\n" + "5 6\n" + "1\n" + "2");
        contexts.add("4 4 1 10 1000\n" + "1\n" + "10\n" + "1\n" + "0 0\n" + "5\n" + "1\n" + "2 2\n" + "2\n" + "0 0");
        contexts.add("4 4 1 10 1000\n" + "1\n" + "10\n" + "1\n" + "0 0\n" + "5\n" + "1\n" + "1 2\n" + "3\n" + "0 1 1");
        contexts.add("4 4 1 10 1000\n" + "1\n" + "10\n" + "1\n" + "1 1\n" + "5\n" + "1\n" + "1 2\n" + "3\n" + "0 1 1");
        contexts.add("4 4 1 10 10000\n" + "1\n" + "10\n" + "1\n" + "0 0\n" + "5\n" + "1\n" + "2 2\n" + "1\n" + "1");
        contexts.add("7 7 2 25 500\n" + "3\n" + "100 5 450\n" + "2\n" + "0 0\n" + "5 1 0\n" + "5 5\n" + "0 10 2\n" +
                "3\n" + "1 1\n" + "2\n" + "0 1\n" + "3 3\n" + "1\n" + "0\n" + "5 6\n" + "1\n" + "1");
        contexts.add("4 4 1 10 10000\n" + "1\n" + "10\n" + "1\n" + "0 0\n" + "5\n" + "4\n" + "2 2\n" + "1\n" + "1\n" +
                "3 2\n" + "2\n" + "2 2\n" + "5 2\n" + "5\n" + "0 0 0 0 0\n" + "1 2\n" + "1\n" + "1");
        contexts.add("4 4 1 10 10000\n" + "1\n" + "10\n" + "1\n" + "0 0\n" + "500\n" + "2\n" + "2 2\n" + "10\n" +
                "0 0 0 0 0 0 0 0 0 0\n" + "2 1\n" + "25\n" + "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0");
		}
	
    @Test
    public void oneDroneWholeContext() throws IOException, BadWeightException, SyntaxErrorException {
        StringBuilder res = new StringBuilder();
        for (String str : contexts) {
            Parser parser = ParserTest.setUp(str, folder.newFile());
            parser.parse();
            Scheduler scheduler = new Scheduler(parser);
            scheduler.setContext(new StrategyOneDrone());
            File file = folder.newFile("scheduler.out");
            scheduler.writeContext(file);
            System.out.println(new Scanner(file).useDelimiter("\\Z").next());
        }
    }

    @Test
    public void OneDroneMaxChargeContext() {

    }