# D�finition des contextes

### Contexte 1

##### Description
Ce contexte utilise 3 drones, contient beaucoup d'entrep�ts (5) ainsi que beaucoup de commande � livrer (6). Chaque commande et chaque entrep�ts sont bien r�partis sur toute la carte. Il y a 5 types de produits avec des poids allant de valeurs tr�s faibles � des valeurs tr�s �lev�es. Au total de 59 produits sont � livrer.

##### Int�r�t du contexte
C'est un contexte tr�s complet et tr�s g�n�ral qui utilise un nombre moyen de drones, avec un temps raisonnable pour r�aliser l'ensemble des commandes.

### Contexte 2

##### Description
Ce contexte utilise 1 seul drone pouvant transporter jusqu'� un poids de 500. Il y a beaucoup d'entrep�ts (5) disponibles ainsi que beaucoup de commande � livrer (7). Chaque commande et chaque entrep�ts sont correctement r�partis sur l'ensemble de la carte. Il y a 6 types de produits de faible poids. Au total 80 produits sont � livrer.

##### Int�r�t du contexte
Ce contexte permet de v�rifier le comportement d'une strat�gie quand elle n'a qu'un drone � disposition pour beaucoup de commande � livrer. Le temps est plut�t limit� en vue du travail que doit r�aliser ce seul et unique drone.

### Contexte 3

##### Description
Ce contexte utilise 6 drones, chacun pouvant chacun charger jusqu'� un poids de 500. Plusieurs entrep�ts (5) sont � dispositions pour r�aliser au total (7) commandes. Les commandes et les entrep�ts sont correctement r�partis sur toute la map. Au total il y a 6 types de produits diff�rents de faible poids. Au total 80 produits sont � livrer.

##### Int�r�t du contexte
Ce contexte permet de v�rifier le comportement d'une strat�gie quand elle dispose de plusieurs drones avec des produits de faibles poids � livrer. Le temps pour r�aliser toutes les commandes devrait �tre suffisant en vue du nombres de drones mis � disposition ainsi que le poids des produits � livrer comparer au poids que peut supporter chaque drone.