package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.deliverystrategy.StrategyOneDrone;
import fr.unice.polytech.si3.drone.deliverystrategy.StrategyWait;
import fr.unice.polytech.si3.drone.deliverystrategy.StrategyWholeFleet;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

/**
 * Created by Florian on 16/01/2017.
 */
public class SchedulerTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Before
    public void SetUp() {
        Parser.MAX_TURNS = 200;
        Parser.MAX_PAYLOAD = 200;
    }

    public Parser parser(String content, File file) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(content);
        bw.close();
        return new Parser(file);
    }

    @Test
    public void writeMap() throws Exception {
        String content = "7 5 2 50 500 \n" + "3 \n" + "100 5 450 \n" + "1 \n" + "0 0 \n" + "5 1 0 \n" + "1 \n"
                + "1 1 \n" + "2 \n" + "2 0";
        Parser parser = parser(content, folder.newFile());
        parser.parse();
        Scheduler scheduler = new Scheduler(parser);
        File file = folder.newFile("map.csv");
        scheduler.writeMap(file);
        assertEquals("W0,,,,\n" +
                ",O0,,,\n" +
                ",,,,\n" +
                ",,,,\n" +
                ",,,,\n" +
                ",,,,\n" +
                ",,,,", new Scanner(file).useDelimiter("\\Z").next());
    }

    @Test
    public void writeContext() throws Exception {
        String content = "7 5 2 25 500 \n" + "3 \n" + "100 5 450 \n" + "1 \n" + "0 0 \n" + "5 1 1 \n" + "1 \n"
                + "1 1 \n" + "2 \n" + "2 0";
        Parser parser = parser(content, folder.newFile());
        parser.parse();
        Scheduler scheduler = new Scheduler(parser);
        scheduler.setContext(new StrategyWait());
        File file = folder.newFile("Scheduler.out");
        scheduler.writeContext(file);
        assertEquals("2\n" +
                "0 W 25\n" +
                "1 W 25", new Scanner(file).useDelimiter("\\Z").next());
    }

    @After
    public void reset() {
        Parser.MAX_PAYLOAD = 0;
        Parser.MAX_TURNS = 0;
    }
}