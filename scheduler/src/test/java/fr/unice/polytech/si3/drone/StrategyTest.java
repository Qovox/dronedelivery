package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.deliverystrategy.*;
import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openjdk.jmh.annotations.Benchmark;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StrategyTest {

    private List<Drone> drones;
    private List<Warehouse> warehouses;
    private List<Order> orders;
    private DeliveryStrategy strategy;

    @Before
    public void setUp() {
        Parser.MAX_PAYLOAD = 100;
        Parser.MAX_TURNS = 100;
    }

    private void testStrategy1() throws BadWeightException {
        drones = new ArrayList<>();
        drones.add(new Drone(28, new HashMap<>(), new Coord()));
        Map<Product, Integer> stuff = new HashMap<>();
        Map<Product, Integer> order = new HashMap<>();
        stuff.put(new Product(45, 50), 1);
        order.put(new Product(45, 50), 1);
        warehouses = new ArrayList<>();
        warehouses.add(new Warehouse(4, stuff, new Coord(3, 0)));
        orders = new ArrayList<>();
        orders.add(new Order(order, new Coord(69, 2), 0));
    }

    private void testStrategy2() throws BadWeightException {
        drones = new ArrayList<>();
        drones.add(new Drone(12, new HashMap<>(), new Coord()));
        drones.add(new Drone(13, new HashMap<>(), new Coord()));
        Map<Product, Integer> stuff = new HashMap<>();
        Map<Product, Integer> stuff1 = new HashMap<>();
        stuff.put(new Product(44, 20), 3);
        stuff.put(new Product(45, 50), 1);
        stuff1.put(new Product(44, 20), 3);
        stuff1.put(new Product(45, 50), 1);
        warehouses = new ArrayList<>();
        warehouses.add(new Warehouse(4, stuff, new Coord(3, 0)));
        orders = new ArrayList<>();
        orders.add(new Order(stuff1, new Coord(10, 2), 0));
    }

    private void testStrategy3() throws BadWeightException {
        drones = new ArrayList<>();
        drones.add(new Drone(12, new HashMap<>(), new Coord()));
        drones.add(new Drone(13, new HashMap<>(), new Coord()));
        Map<Product, Integer> order = new HashMap<>();
        Map<Product, Integer> order2 = new HashMap<>();
        Map<Product, Integer> stock = new HashMap<>();
        Map<Product, Integer> stock2 = new HashMap<>();
        order.put(new Product(44, 20), 2);
        order2.put(new Product(45, 20), 1);
        stock.put(new Product(44, 20), 1);
        stock2.put(new Product(44, 20), 1);
        stock2.put(new Product(45, 20), 1);
        warehouses = new ArrayList<>();
        warehouses.add(new Warehouse(4, stock, new Coord(3, 0)));
        warehouses.add(new Warehouse(5, stock2, new Coord(3, 4)));
        orders = new ArrayList<>();
        orders.add(new Order(order, new Coord(10, 2), 2));
        orders.add(new Order(order2, new Coord(10, 4), 3));
    }

    @Test
    public void testStrategyOneDroneNoTime() throws BadWeightException {
        Parser.MAX_TURNS = 2;
        testStrategy1();
        strategy = new StrategyOneDrone();
        assertEquals("", strategy.apply(warehouses, drones, orders));
        Parser.MAX_TURNS = 100;
    }

    @Test
    public void testSttrategyWholeFleetNoTime() throws BadWeightException {
        Parser.MAX_TURNS = 2;
        testStrategy1();
        strategy = new StrategyWholeFleet();
        assertEquals("", strategy.apply(warehouses, drones, orders));
        Parser.MAX_TURNS = 100;
    }

    @Test
    public void testSttrategyOneProductNoTime() throws BadWeightException {
        Parser.MAX_TURNS = 2;
        testStrategy1();
        strategy = new StrategyOneProduct();
        assertEquals("", strategy.apply(warehouses, drones, orders));
        Parser.MAX_TURNS = 100;
    }

    @Test
    public void testSttrategyOneDroneMaxChargeNoTime() throws BadWeightException {
        Parser.MAX_TURNS = 2;
        testStrategy1();
        strategy = new StrategyOneDroneMaxCharge();
        assertEquals("", strategy.apply(warehouses, drones, orders));
        Parser.MAX_TURNS = 100;
    }


    @Test
    public void testStrategyOneDrone1() throws BadWeightException {
        testStrategy1();
        strategy = new StrategyOneDrone();
        assertEquals("28 L 4 45 1\n28 D 0 45 1\n", strategy.apply(warehouses, drones, orders));
    }

    @Test
    public void testStrategyWholeFleet1() throws BadWeightException {
        testStrategy1();
        strategy = new StrategyWholeFleet();
        assertEquals("28 L 4 45 1\n28 D 0 45 1\n", strategy.apply(warehouses, drones, orders));
    }

    @Test
    public void testStrategyOneDrone2() throws BadWeightException {
        testStrategy2();
        strategy = new StrategyOneDrone();
        String res = strategy.apply(warehouses, drones, orders);
        String possible1 = "12 L 4 45 1\n" +
                "12 D 0 45 1\n" +
                "12 L 4 44 1\n" +
                "12 D 0 44 1\n" +
                "12 L 4 44 1\n" +
                "12 D 0 44 1\n" +
                "12 L 4 44 1\n" +
                "12 D 0 44 1\n";
        String possible2 = "12 L 4 44 1\n" +
                "12 D 0 44 1\n" +
                "12 L 4 44 1\n" +
                "12 D 0 44 1\n" +
                "12 L 4 44 1\n" +
                "12 D 0 44 1\n" +
                "12 L 4 45 1\n" +
                "12 D 0 45 1\n";
        assertTrue(possible2.equals(res) || possible1.equals(res));
    }

    @Test
    public void testStrategyWholeFleet() throws BadWeightException {
        testStrategy2();
        strategy = new StrategyWholeFleet();
        assertEquals("12 L 4 44 1\n" +
                "13 L 4 44 1\n" +
                "12 D 0 44 1\n" +
                "13 D 0 44 1\n" +
                "12 L 4 44 1\n" +
                "13 L 4 45 1\n" +
                "12 D 0 44 1\n" +
                "13 D 0 45 1\n", strategy.apply(warehouses, drones, orders));
    }

    @Test
    public void testStrategyOneDrone3() throws BadWeightException {
        testStrategy3();
        strategy = new StrategyOneDrone();
        String res = strategy.apply(warehouses, drones, orders);
        String possible1 = "12 L 5 45 1\n" +
                "12 D 3 45 1\n" +
                "12 L 4 44 1\n" +
                "12 D 2 44 1\n" +
                "12 L 5 44 1\n" +
                "12 D 2 44 1\n";
        String possible2 = "12 L 4 44 1\n" +
                "12 D 2 44 1\n" +
                "12 L 5 44 1\n" +
                "12 D 2 44 1\n" +
                "12 L 5 45 1\n" +
                "12 D 3 45 1\n";
        assertTrue(possible1.equals(res) || possible2.equals(res));
    }

    @Test
    public void testStrategyOneDroneMaxCharge1() throws BadWeightException {
        testStrategy1();
        strategy = new StrategyOneDroneMaxCharge();
        String res = strategy.apply(warehouses, drones, orders);
        String possible1 = "28 L 4 45 1\n" +
                "28 D 0 45 1\n";
        assertTrue(possible1.equals(res));
    }

    @Test
    public void testStrategyOneDroneMaxCharge2() throws BadWeightException {
        testStrategy2();
        strategy = new StrategyOneDroneMaxCharge();
        String res = strategy.apply(warehouses, drones, orders);
        String possible1 = "12 L 4 44 3\n" +
                "12 D 0 44 3\n" +
                "12 L 4 45 1\n" +
                "12 D 0 45 1\n";
        assertTrue(possible1.equals(res));
    }

    @Test
    public void testStrategyOneDroneMaxCharge3() throws BadWeightException {
        testStrategy3();
        strategy = new StrategyOneDroneMaxCharge();
        String res = strategy.apply(warehouses, drones, orders);
        String possible1 = "12 L 4 44 1\n" +
                "12 L 5 44 1\n" +
                "12 L 5 45 1\n" +
                "12 D 2 44 2\n" +
                "12 D 3 45 1\n";
        assertTrue(possible1.equals(res));
    }

    @Test
    public void testStrategyOneProduct1() throws BadWeightException {
        testStrategy1();
        strategy = new StrategyOneProduct();
        assertEquals("28 L 4 45 1\n28 D 0 45 1\n", strategy.apply(warehouses, drones, orders));
    }

    @Test
    public void testStrategyOneProduct2() throws BadWeightException {
        testStrategy2();
        strategy = new StrategyOneProduct();
        String res = strategy.apply(warehouses, drones, orders);
        String possible1 = "12 L 4 44 3\n" +
                "13 L 4 45 1\n" +
                "12 D 0 44 3\n" +
                "13 D 0 45 1\n";
        assertEquals(possible1, res);
    }

    @Test
    public void testStrategyOneProduct3() throws BadWeightException {
        testStrategy3();
        strategy = new StrategyOneProduct();
        String res = strategy.apply(warehouses, drones, orders);
        String possible1 = "12 L 4 44 1\n" +
                "12 L 5 44 1\n" +
                "13 L 5 45 1\n" +
                "13 D 3 45 1\n" +
                "12 D 2 44 2\n";
        String possible2 = "12 L 4 44 1\n" +
                "12 L 5 44 1\n" +
                "13 L 5 45 1\n" +
                "12 D 2 44 2\n" +
                "13 D 3 45 1\n";
        assertTrue(possible1.equals(res) || possible2.equals(res));
    }


    @After
    public void reset() {
        Parser.MAX_PAYLOAD = 0;
        Parser.MAX_TURNS = 0;
    }
}
