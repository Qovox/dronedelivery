package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.deliverystrategy.DeliveryStrategy;
import fr.unice.polytech.si3.drone.deliverystrategy.StrategyWait;
import fr.unice.polytech.si3.drone.exception.BadWeightException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class StrategyWaitTest {

    private List<Drone> drones;
    private List<Warehouse> warehouses;
    private List<Order> orders;
    private DeliveryStrategy strategy;

    @Before
    public void setUp() {
        Parser.MAX_PAYLOAD = 100;
    }

    @Test
    public void testStrategyWait() throws BadWeightException {
        Parser.MAX_TURNS = 10;
        drones = Collections.singletonList(new Drone(0, new HashMap<>(), new Coord()));
        Map<Product, Integer> stuff = new HashMap<>();
        stuff.put(new Product(0, 50), 1);
        warehouses = Collections.singletonList(new Warehouse(0, stuff, new Coord()));
        orders = Collections.singletonList(new Order(stuff, new Coord(1, 0), 0));
        strategy = new StrategyWait();
        assertEquals("0 W 10\n", strategy.apply(warehouses, drones, orders));
    }

    @Test
    public void testStrategyWait2() throws BadWeightException {
        Parser.MAX_TURNS = 5;
        drones = Arrays.asList(
                new Drone(0, new HashMap<>(), new Coord()),
                new Drone(1, new HashMap<>(), new Coord()),
                new Drone(3, new HashMap<>(), new Coord())
        );
        Map<Product, Integer> stuff = new HashMap<>();
        stuff.put(new Product(0, 50), 1);
        warehouses = Collections.singletonList(new Warehouse(0, stuff, new Coord()));
        orders = Collections.singletonList(new Order(stuff, new Coord(1, 0), 0));
        strategy = new StrategyWait();
        assertEquals("0 W 5\n1 W 5\n3 W 5\n", strategy.apply(warehouses, drones, orders));
    }

    @After
    public void reset() {
        Parser.MAX_PAYLOAD = 0;
        Parser.MAX_TURNS = 0;
    }
}
