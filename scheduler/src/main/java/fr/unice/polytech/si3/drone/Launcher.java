package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;
import fr.unice.polytech.si3.drone.exception.SyntaxErrorException;
import org.openjdk.jmh.runner.RunnerException;

import java.io.File;
import java.io.IOException;

/**
 * Launcher class. Handles initialization.
 */
public class Launcher {

    private File file;
    private boolean benchmarkOn = false;
    private boolean profiling = false;
    private String arg;

    public Launcher(String[] args) {
        if (args.length == 0) {
            System.out.println("No input file found, please specify a correct input file");
            System.exit(5);
        }
        if (args.length == 2 && !("-benchmark".equals(args[1]) || "-profile".equals(args[1]))) {
            System.out.println("Bad argument. The only available options are : \"-benchmark\" and \"-profile\"");
            System.exit(5);
        } else if (args.length == 2 && "-benchmark".equals(args[1])) {
            benchmarkOn = true;
        } else if (args.length == 2 && "-profile".equals(args[1])) {
            profiling = true;
        }
        if (args.length > 2) {
            System.out.println("Too many options. The only available options are : \"-benchmark and  -profile\"");
            System.exit(5);
        }
        file = new File(args[0]);
        arg = args[0];
    }

    /**
     * Schedule orders with drones
     */
    public void execute() throws IOException, SyntaxErrorException,
            BadWeightException, OutOfTimeException, EmptyInventoryException, RunnerException {
        Parser parser = new Parser(file);
        parser.parse();
        Scheduler scheduler = new Scheduler(parser);
        scheduler.writeMap();
        if (benchmarkOn) {
            scheduler.writeAllContexts(file, false);
            MetricsBenchmark.benchMark(arg);
        } else if (profiling) {
            scheduler.writeAllContexts(file, true);
        } else {
            scheduler.writeContext();
        }
    }
}
