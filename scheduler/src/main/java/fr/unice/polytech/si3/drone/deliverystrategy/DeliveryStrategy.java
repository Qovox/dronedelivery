package fr.unice.polytech.si3.drone.deliverystrategy;

import fr.unice.polytech.si3.drone.Drone;
import fr.unice.polytech.si3.drone.Order;
import fr.unice.polytech.si3.drone.Warehouse;
import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;

import java.util.List;

/**
 * @author Florian Bourniquel
 * @version 1.0
 *          <p>
 *          Interface for any type of delivery strategy
 */
public abstract class DeliveryStrategy {

    protected StringBuilder res = new StringBuilder();
    protected String line;

    /**
     * method to set up the strategy
     *
     * @param warehouses list of the warehouses, containing their products
     * @param drones     list of available drones
     * @param orders     list of order to satisfy
     * @return String containing instructions
     */
    public abstract String apply(List<Warehouse> warehouses, List<Drone> drones, List<Order> orders);

    /**
     * Updates the drone on a turn
     *
     * @param drone - the drone to update
     */
    protected void updateDrone(Drone drone) {
        try {
            line = drone.update();
            if (!line.equals("")) {
                res.append(line).append('\n');
                drone.reset();
            }
        } catch (BadWeightException | EmptyInventoryException | NotEnoughItemsException e) {
            e.printStackTrace();
        }
    }


}
