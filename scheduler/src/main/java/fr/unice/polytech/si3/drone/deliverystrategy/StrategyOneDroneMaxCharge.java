package fr.unice.polytech.si3.drone.deliverystrategy;

import fr.unice.polytech.si3.drone.Drone;
import fr.unice.polytech.si3.drone.Order;
import fr.unice.polytech.si3.drone.Product;
import fr.unice.polytech.si3.drone.Warehouse;
import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static fr.unice.polytech.si3.drone.Parser.MAX_PAYLOAD;
import static fr.unice.polytech.si3.drone.Parser.MAX_TURNS;

/**
 * One drone is used to complete the orders. The drone
 * try to load the most items it can carry before starting
 * to deliver.
 */
public class StrategyOneDroneMaxCharge extends DeliveryStrategy {

    private boolean deliver = false;

    /**
     * @param warehouses list of the warehouses, containing their products
     * @param drones     list of available drones
     * @param orders     list of order to satisfy
     * @return String containing all the actions done
     */
    @Override
    public String apply(List<Warehouse> warehouses, List<Drone> drones, List<Order> orders) {

        if (drones.isEmpty()) {
            return "";
        }
        Drone drone = drones.get(0);
        for (int i = 0; i < MAX_TURNS && !orders.isEmpty() ; i++) {
            if (drone.isAvailable() && (!deliver || drone.getCarriedProducts().isEmpty())) {
                deliver = false;
                load(warehouses, orders, drone, i);
                updateDrone(drone);
            } else if (drone.isAvailable() && deliver) {
                deliverProduct(orders, drone, i);
                updateDrone(drone);
            } else updateDrone(drone);
            checkLists(orders, warehouses);
        }
        return res.toString();
    }

    public void load(List<Warehouse> warehouses, List<Order> orders, Drone drone, int currentTurn) {
        for (Warehouse warehouse : warehouses) {
            for (Order order : orders) {
                for (Product product : order.getLoaded().keySet()) {
                    if (warehouse.getProducts().containsKey(product) &&
                            (drone.getWeight() + product.getWeight()) <= MAX_PAYLOAD) {
                        try {
                            int quantity = (MAX_PAYLOAD - drone.getWeight()) / (product.getWeight());
                            if (quantity > order.getContents().get(product))
                                quantity = order.getContents().get(product);
                            if (quantity > warehouse.getProducts().get(product))
                                quantity = warehouse.getProducts().get(product);
                            drone.load(warehouse, product, quantity, MAX_TURNS - currentTurn);
                            order.removeProductLoaded(product, quantity);
                            return;
                        } catch (BadWeightException | EmptyInventoryException | OutOfTimeException | NotEnoughItemsException e) {
                            return;
                        }
                    } else if ((drone.getWeight() + getMinWeight(orders)) > MAX_PAYLOAD){
                        deliver = true;
                        deliverProduct(orders, drone, currentTurn);
                        return;
                    }
                }
            }
        }
        deliver = true;
        deliverProduct(orders, drone, currentTurn);
        return;
    }

    public void deliverProduct(List<Order> orders, Drone drone, int currentTurn) {
        for (Order order : orders) {
            for (Product product : drone.getCarriedProducts().keySet()) {
                if (order.getContents().containsKey(product)) {
                    try {
                        int quantity = 0;
                        if (order.getContents().get(product) > drone.getCarriedProducts().get(product)) {
                            quantity = drone.getCarriedProducts().get(product);
                        } else quantity = order.getContents().get(product);
                        drone.deliver(order, product, quantity, MAX_TURNS - currentTurn);
                        return;
                    } catch (EmptyInventoryException | OutOfTimeException e) {
                        return;
                    }
                }
            }
        }
        deliver = true;
        deliverProduct(orders, drone, currentTurn);
        return;
    }

    public int getMinWeight(List<Order> orders) {
        int min = MAX_PAYLOAD;
        for (Order order : orders) {
            for(Product product : order.getContents().keySet()) {
                if (product.getWeight() < min) {
                    min = product.getWeight();
                }
            }
        }
        return min;
    }

    public void checkLists(List<Order> orders, List<Warehouse> warehouses) {
        Iterator<Order> itO = orders.iterator();
        while (itO.hasNext()) {
            Order order = itO.next();
            if (order.getContents().isEmpty()) {
                itO.remove();
            }
        }
        Iterator<Warehouse> itW = warehouses.iterator();
        while (itW.hasNext()) {
            Warehouse warehouse = itW.next();
            if (warehouse.getProducts().isEmpty()) {
                itW.remove();
            }
        }
    }

}