package fr.unice.polytech.si3.drone.deliverystrategy;

import fr.unice.polytech.si3.drone.Drone;
import fr.unice.polytech.si3.drone.Order;
import fr.unice.polytech.si3.drone.Warehouse;

import java.util.List;

/**
 * @author Florian Bourniquel
 * @version 1.0
 *          <p>
 *          Context for delivery strategy
 */
public class Context {

    public static int CPT = 0;

    private DeliveryStrategy deliveryStrategy;

    /**
     * Standard constructor
     *
     * @param deliveryStrategy - the strategy associated to this context
     */
    public Context(DeliveryStrategy deliveryStrategy) {
        this.deliveryStrategy = deliveryStrategy;
    }

    /**
     * method to set up the strategy
     *
     * @param warehouses list of the warehouses, containing their products
     * @param drones     list of available drones
     * @param orders     list of order to satisfy
     */
    public String apply(List<Warehouse> warehouses, List<Drone> drones, List<Order> orders) {
        return deliveryStrategy.apply(warehouses, drones, orders);
    }

}
