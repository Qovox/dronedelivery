package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.deliverystrategy.StrategyOneDrone;
import fr.unice.polytech.si3.drone.deliverystrategy.StrategyOneDroneMaxCharge;
import fr.unice.polytech.si3.drone.deliverystrategy.StrategyOneProduct;
import fr.unice.polytech.si3.drone.deliverystrategy.StrategyWholeFleet;
import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.SyntaxErrorException;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Class for the benchmarks of strategies (not really testable + Not dynamix due to JMH framework)
 */
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
@State(Scope.Benchmark)
public class MetricsBenchmark {

    private Parser parser;
    private Scheduler scheduler;

    @Param({"invalid"})
    private String arg;

    @Setup
    public void setup() throws SyntaxErrorException, IOException, BadWeightException {
        System.out.println("...");
    }

    @Benchmark
    public void OneDrone() throws IOException, SyntaxErrorException, BadWeightException {
        File file = new File(arg);
        parser = new Parser(file);
        parser.parse();
        scheduler = new Scheduler(parser);
        scheduler.reset(parser);
        scheduler.setContext(new StrategyOneDrone());
        scheduler.executeStrategy();
    }

    @Benchmark
    public void WholeFleet() throws IOException, SyntaxErrorException, BadWeightException {
        File file = new File(arg);
        parser = new Parser(file);
        parser.parse();
        scheduler = new Scheduler(parser);
        scheduler.reset(parser);
        scheduler.setContext(new StrategyWholeFleet());
        scheduler.executeStrategy();
    }

    @Benchmark
    public void OneDroneMaxCharge() throws IOException, SyntaxErrorException, BadWeightException {
        File file = new File(arg);
        parser = new Parser(file);
        parser.parse();
        scheduler = new Scheduler(parser);
        scheduler.reset(parser);
        scheduler.setContext(new StrategyOneDroneMaxCharge());
        scheduler.executeStrategy();
    }

    @Benchmark
    public void OneProduct() throws IOException, SyntaxErrorException, BadWeightException {
        File file = new File(arg);
        parser = new Parser(file);
        parser.parse();
        scheduler = new Scheduler(parser);
        scheduler.reset(parser);
        scheduler.setContext(new StrategyOneProduct());
        scheduler.executeStrategy();
    }

    /**
     * Main function to launch the benchmarks
     * @param arg String given by main (name of the context file)
     * @throws RunnerException error while benchmarking
     */
    public static void benchMark(String arg) throws RunnerException {
        System.out.println("Schedule is done\nBenchMarking...");
        Options opt = new OptionsBuilder()
                .include(".*" + MetricsBenchmark.class.getSimpleName() + ".*")
                .output("exectime.txt")
                .param("arg", arg)
                .build();

        new Runner(opt).run();
        System.out.println("Done");
    }

}