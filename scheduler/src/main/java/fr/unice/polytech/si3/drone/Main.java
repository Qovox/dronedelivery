package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;
import fr.unice.polytech.si3.drone.exception.SyntaxErrorException;
import org.openjdk.jmh.runner.RunnerException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

public class Main {

    public static void main(String[] args) throws IOException, SyntaxErrorException,
            BadWeightException, OutOfTimeException, EmptyInventoryException, RunnerException {
        URLClassLoader classLoader = (URLClassLoader) fr.unice.polytech.si3.drone.Main.class.getClassLoader();
        StringBuilder classpath = new StringBuilder();
        for(URL url : classLoader.getURLs())
            classpath.append(url.getPath()).append(File.pathSeparator);
        System.setProperty("java.class.path", classpath.toString());
        Launcher launcher = new Launcher(args);
        launcher.execute();
    }

}
