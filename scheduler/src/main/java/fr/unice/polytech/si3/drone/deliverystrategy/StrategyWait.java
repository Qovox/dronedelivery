package fr.unice.polytech.si3.drone.deliverystrategy;


import fr.unice.polytech.si3.drone.Drone;
import fr.unice.polytech.si3.drone.Order;
import fr.unice.polytech.si3.drone.Parser;
import fr.unice.polytech.si3.drone.Warehouse;
import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;

import java.util.List;

/**
 * Most basic strategy, just waits until the end of turns
 */
public class StrategyWait extends DeliveryStrategy {

    /**
     * Most basic strategy, just makes the drone wait
     *
     * @param warehouses list of the warehouses, containing their products
     * @param drones     list of available drones
     * @param orders     list of order to satisfy
     * @return String containing all the actions done
     */
    @Override
    public String apply(List<Warehouse> warehouses, List<Drone> drones, List<Order> orders) {
        StringBuilder res = new StringBuilder();
        for (Drone drone : drones) {
            if (drone.isAvailable()) {
                try {
                    res.append(drone.wait(Parser.MAX_TURNS, Parser.MAX_TURNS)).append('\n');
                } catch (OutOfTimeException e) {
                    return res.toString();
                }
            }
            try {
                drone.update();
            } catch (BadWeightException | EmptyInventoryException | NotEnoughItemsException e) {
                return res.toString();
            }
        }
        return res.toString();
    }
}
