package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.deliverystrategy.*;
import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.SyntaxErrorException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Florian Bourniquel
 * @version 1.0
 *
 *          System exits:
 *          0 - Everything went right
 *          1 - Incorrect Weight for a product
 *          2 - Syntax error in input file
 *          3 - Load items on empty inventory
 *          4 - Not enough turns left
 */
public class Scheduler {

    private List<Drone> drones;
    private List<Warehouse> warehouses;
    private List<Order> orders;
    private Context context;

    /**
     * Standard constructor
     *
     * @param parser - the parser to read the initialization data
     */
    public Scheduler(Parser parser) {
        this.drones = parser.getDrones();
        this.warehouses = parser.getWarehouses();
        this.orders = parser.getOrders();
        this.context = (drones.size() == 1)
                ? new Context(new StrategyOneDroneMaxCharge())
                : new Context(new StrategyOneProduct());
    }

    /**
     * Resets data for a new strategy
     * @param parser - the parser to read data from
     */
    public void reset(Parser parser) {
        this.drones =parser.getDrones();
        this.warehouses = parser.getWarehouses();
        this.orders =parser.getOrders();
    }

    /**
     * Creates a table corresponding to a map of all warehouses and destinations for orders.
     *
     * @throws IOException - if failing to write the file
     */
    public void writeMap(File file) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        for (int i = 0; i < Parser.DIMENSIONS.getX(); i++) {
            for (int j = 0; j < Parser.DIMENSIONS.getY(); j++) {
                for (Warehouse warehouse:warehouses) {
                    if (warehouse.getLocation().equals(new Coord(i,j))) {
                        bw.write("W" + warehouse.getId());
                    }
                }
                for (Order order : orders) {
                    if (order.getDestination().equals(new Coord(i, j)))
                        bw.write("O" + order.getId());
                }
                if (j != Parser.DIMENSIONS.getY()-1)
                    bw.write(',');
            }
            bw.write('\n');
        }
        bw.close();
    }

    /**
     * Creates a table corresponding to a map of all warehouses and destinations for orders.
     *
     * @throws IOException - if failing to write the file
     */
    public void writeMap() throws IOException {
        writeMap(new File("map.csv"));
    }

    /**
     * Write code corresponding to the instructions
     *
     * @throws IOException - if failing to write the file
     * @return File - the file written
     */
    public File writeContext(File file) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        String text = context.apply(warehouses, drones, orders);
        long commands = text.chars().filter(ch -> ch == '\n').count();
        bw.write(commands + "\n");
        bw.write(text);
        bw.close();
        return file;
    }

    /**
     * Just executes the strategy corresponding to the instructions
     *
     * @throws IOException - if failing to write the file
     */
    public void executeStrategy() throws IOException {
        context.apply(warehouses, drones, orders);
    }

    /**
     * For benchmark use, writes the results of all strategies in the strategies enum in a file named after the strategy
     *
     * @return List of Files - the files written into for each strategy
     * @throws IOException - if failing to write a file
     */
    public List<File> writeAllContexts(File file, boolean profiling) throws IOException, BadWeightException, SyntaxErrorException {
        List<File> files = new ArrayList<>();
        for (Strategies strategie : Strategies.values()) {
            Parser parser = new Parser(file);
            parser.parse();
            reset(parser);
            if (profiling) {
                setContext(new StrategyProfiler(strategie.get()));
            }
            else {
                setContext(strategie.get());
            }
            files.add(writeContext(new File(strategie.name() + ".out")));
        }
        return files;
    }

    /**
     * Write code corresponding to the instructions
     *
     * @throws IOException - if failing to write the file
     * @return File - the file written
     */
    public File writeContext() throws IOException {
        return writeContext(new File("scheduler.out"));
    }

    /**
     * Method to change the current DeliveryStrategy to another one
     *
     * @param strat sets current strategy as this one
     */
    public void setContext(DeliveryStrategy strat) {
        this.context = new Context(strat);
    }

}
