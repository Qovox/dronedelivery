package fr.unice.polytech.si3.drone.deliverystrategy;

import fr.unice.polytech.si3.drone.Drone;
import fr.unice.polytech.si3.drone.Order;
import fr.unice.polytech.si3.drone.Product;
import fr.unice.polytech.si3.drone.Warehouse;
import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.unice.polytech.si3.drone.Parser.MAX_TURNS;

/**
 * Take into account the whole fleet and make each drone
 * deliver one item at a time
 */
public class StrategyWholeFleet extends DeliveryStrategy {

    private Map<Drone, Order> droneOrder;
    private Map<Drone, Warehouse> droneWarehouse;
    private Map<Drone, Product> droneCurrentProduct;

    /**
     * @param warehouses list of the warehouses, containing their products
     * @param drones     list of available drones
     * @param orders     list of order to satisfy
     * @return String containing all the actions done
     */
    @Override
    public String apply(List<Warehouse> warehouses, List<Drone> drones, List<Order> orders) {
        droneOrder = new HashMap<>();
        droneWarehouse = new HashMap<>();
        droneCurrentProduct = new HashMap<>();

        if (drones.isEmpty()) {
            return "";
        }

        for (int i = 0; i < MAX_TURNS; i++) {
            for (Drone drone : drones) {
                if (drone.isAvailable() && droneWarehouse.get(drone) == null) {
                    for (Order order : orders) {
                        if (!order.getLoaded().isEmpty()) {
                            droneOrder.put(drone, order);
                            break;
                        }
                    }
                    if (droneOrder.get(drone) == null) {
                        continue;
                    }
                    getProductofOrder(drone, warehouses);
                    if (droneWarehouse.get(drone) == null) {
                        continue;
                    }
                    try {
                        drone.load(droneWarehouse.get(drone), droneCurrentProduct.get(drone), 1, MAX_TURNS - i);
                        droneOrder.get(drone).removeProductLoaded(droneCurrentProduct.get(drone), 1);
                        droneWarehouse.get(drone).removeProductLoaded(droneCurrentProduct.get(drone), 1);
                    } catch (BadWeightException | EmptyInventoryException | OutOfTimeException | NotEnoughItemsException e) {
                        return res.toString();
                    }
                    updateDrone(drone);
                }
            }
            for (Drone drone : drones) {
                if (drone.isAvailable() && droneWarehouse.get(drone) != null) {
                    try {
                        drone.deliver(droneOrder.get(drone), droneCurrentProduct.get(drone), 1, MAX_TURNS - i);
                        droneCurrentProduct.remove(drone);
                        if (droneWarehouse.get(drone).getProducts().isEmpty()) {
                            warehouses.remove(droneWarehouse.get(drone));
                        }
                        if (droneOrder.get(drone).getContents().isEmpty()) {
                            orders.remove(droneOrder.get(drone));
                        }
                        droneWarehouse.remove(drone);
                        droneOrder.remove(drone);
                    } catch (EmptyInventoryException | OutOfTimeException e) {
                        return res.toString();
                    }
                    updateDrone(drone);
                }
            }
            conditionalUpdate(drones);
        }
        return res.toString();
    }

    private void getProductofOrder(Drone drone, List<Warehouse> warehouses) {
        for (Warehouse warehouse : warehouses) {
            for (Product product : droneOrder.get(drone).getLoaded().keySet()) {
                if (warehouse.getLoaded().containsKey(product)) {
                    droneWarehouse.put(drone, warehouse);
                    droneCurrentProduct.put(drone, product);
                    return;
                }
            }
        }
    }

    public void conditionalUpdate(List<Drone> drones) {
        for (Drone drone : drones) {
            if (!drone.isAvailable() && droneWarehouse.get(drone) == null) {
                updateDrone(drone);
            }
            if (!drone.isAvailable() && droneWarehouse.get(drone) != null) {
                updateDrone(drone);
            }
        }
    }

}
