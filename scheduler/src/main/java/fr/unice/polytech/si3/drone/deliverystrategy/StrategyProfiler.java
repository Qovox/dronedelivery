package fr.unice.polytech.si3.drone.deliverystrategy;

import fr.unice.polytech.si3.drone.Drone;
import fr.unice.polytech.si3.drone.Order;
import fr.unice.polytech.si3.drone.Warehouse;

import java.util.List;
import java.util.Scanner;

/**
 * Kind of decorator to add a pause at the end of a strategy
 */
public class StrategyProfiler extends DeliveryStrategy {

    private DeliveryStrategy strategy;
    private Scanner sc;

    public StrategyProfiler(DeliveryStrategy strategy) {
        this.strategy = strategy;
        sc = new Scanner(System.in);
    }

    /**
     * Decorates the apply method such that the user can choose when to launch and stop a strategy
     *
     * @param warehouses list of the warehouses, containing their products
     * @param drones     list of available drones
     * @param orders     list of order to satisfy
     * @return result
     */
    @Override
    public String apply(List<Warehouse> warehouses, List<Drone> drones, List<Order> orders) {
        System.out.println("Press any key to start " + strategy.getClass().getSimpleName());
        sc.next();
        String str = strategy.apply(warehouses, drones, orders);
        System.out.println("Press any key to finish " + strategy.getClass().getSimpleName());
        sc.next();
        return str;
    }
}
