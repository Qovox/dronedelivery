package fr.unice.polytech.si3.drone.deliverystrategy;


/**
 * enumeration for the valid strategies we have, this enumeration is used to execute all strategies for benchmark
 */
public enum Strategies {
    OneDrone(new StrategyOneDrone()),
    OneDroneMaxCharge(new StrategyOneDroneMaxCharge()),
    OneProduct(new StrategyOneProduct()),
    WholeFleet(new StrategyWholeFleet());

    private  DeliveryStrategy ds;

    Strategies(DeliveryStrategy ds) {
        this.ds = ds;
    }

    public DeliveryStrategy get() {
        return this.ds;
    }
}
