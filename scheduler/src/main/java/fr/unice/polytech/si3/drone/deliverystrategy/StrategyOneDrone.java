package fr.unice.polytech.si3.drone.deliverystrategy;

import fr.unice.polytech.si3.drone.Drone;
import fr.unice.polytech.si3.drone.Order;
import fr.unice.polytech.si3.drone.Product;
import fr.unice.polytech.si3.drone.Warehouse;
import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;

import java.util.List;

import static fr.unice.polytech.si3.drone.Parser.MAX_TURNS;

/**
 * One drone is used to complete the orders. The drone
 * carry at most one item.
 */
public class StrategyOneDrone extends DeliveryStrategy {

    private Order currentOrder;
    private Warehouse currentWarehouse;
    private Product currentProduct;

    /**
     * @param warehouses list of the warehouses, containing their products
     * @param drones     list of available drones
     * @param orders     list of order to satisfy
     * @return String containing all the actions done
     */
    @Override
    public String apply(List<Warehouse> warehouses, List<Drone> drones, List<Order> orders) {
        if (drones.isEmpty()) {
            return "";
        }
        Drone drone = drones.get(0);
        for (int i = 0; i < MAX_TURNS; i++) {
            if (drone.isAvailable() && currentWarehouse == null) {
                for (Order order : orders) {
                    if (!order.isEmpty()) {
                        currentOrder = order;
                        break;
                    }
                }
                if (currentOrder == null) {
                    break;
                }
                getProductofOrder(warehouses);
                if (currentWarehouse == null) {
                    break;
                }
                try {
                    drone.load(currentWarehouse, currentProduct, 1, MAX_TURNS - i);
                } catch (BadWeightException | EmptyInventoryException | OutOfTimeException e) {
                    return res.toString();
                }
            } else if (drone.isAvailable() && currentWarehouse != null) {
                try {
                    drone.deliver(currentOrder, currentProduct, 1, MAX_TURNS - i);
                    currentProduct = null;
                    if (currentWarehouse.getProducts().isEmpty())
                        warehouses.remove(currentWarehouse);
                    if (currentOrder.getContents().isEmpty())
                        orders.remove(currentOrder);
                    currentOrder = null;
                    currentWarehouse = null;
                } catch (EmptyInventoryException | OutOfTimeException e) {
                    return res.toString();
                }
            }
            updateDrone(drone);
        }
        return res.toString();
    }

    private void getProductofOrder(List<Warehouse> warehouses) {
        for (Warehouse warehouse : warehouses) {
            for (Product p : currentOrder.getContents().keySet()) {
                if (warehouse.getProducts().containsKey(p)) {
                    currentWarehouse = warehouse;
                    currentProduct = p;
                    return;
                }
            }
        }
    }
}
