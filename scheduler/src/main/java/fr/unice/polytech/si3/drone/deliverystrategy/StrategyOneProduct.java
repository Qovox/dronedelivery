package fr.unice.polytech.si3.drone.deliverystrategy;

import fr.unice.polytech.si3.drone.*;
import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;

import java.util.*;

import static fr.unice.polytech.si3.drone.Parser.MAX_PAYLOAD;
import static fr.unice.polytech.si3.drone.Parser.MAX_TURNS;

/**
 * Take into account the whole fleet and make each drone
 * delivere one item at a time
 */
public class StrategyOneProduct extends DeliveryStrategy {

    private Map<Drone, Order> droneOrder;
    private Map<Drone, Warehouse> droneWarehouse;
    private Map<Drone, Product> droneProduct;
    private Inventory productMap;
    private Set<Product> productSet;

    /**
     * @param warehouses list of the warehouses, containing their products
     * @param drones     list of available drones
     * @param orders     list of order to satisfy
     * @return String containing all the actions done
     */
    @Override
    public String apply(List<Warehouse> warehouses, List<Drone> drones, List<Order> orders) {
        droneOrder = new HashMap<>();
        droneWarehouse = new HashMap<>();
        droneProduct = new HashMap<>();
        productMap = new Inventory(new HashMap<>());
        productSet = new HashSet<>();

        if (drones.isEmpty()) {
            return "";
        }

        for (Order order : orders) {
            for (Product p : order.getContents().keySet()) {
                if (productMap.containsKey(p)) {
                    productMap.put(p, order.getContents().get(p) + productMap.get(p));
                } else {
                    productMap.put(p, order.getContents().get(p));
                    productSet.add(p);
                }
            }
        }

        for (int i = 0; i < MAX_TURNS; i++) {
            for (Drone drone : drones) {
                if (drone.isAvailable() && droneWarehouse.get(drone) == null) {
                    if (droneProduct.get(drone) == null) {
                        assignProduct(drone);
                    }
                    if (droneProduct.get(drone) == null) {
                        continue;
                    }
                    getWarehouse(drone, warehouses);
                    if (droneWarehouse.get(drone) == null || productMap.isEmpty() || !productMap.containsKey(droneProduct.get(drone))) {
                        continue;
                    }
                    try {
                        int quantity = productMap.get(droneProduct.get(drone));
                        if (quantity > MAX_PAYLOAD / droneProduct.get(drone).getWeight())
                            quantity = MAX_PAYLOAD / droneProduct.get(drone).getWeight();
                        if (quantity > droneWarehouse.get(drone).getProducts().get(droneProduct.get(drone)))
                            quantity = droneWarehouse.get(drone).getProducts().get(droneProduct.get(drone));
                        drone.load(droneWarehouse.get(drone), droneProduct.get(drone), quantity, MAX_TURNS - i);
                        productMap.removeContents(droneProduct.get(drone), quantity);
                        if ((MAX_PAYLOAD - (droneProduct.get(drone).getWeight()*quantity + drone.getWeight()) > droneProduct.get(drone).getWeight()) &&
                                productMap.get(droneProduct.get(drone)) != null) {
                            droneWarehouse.remove(drone);
                        }
                    } catch (BadWeightException | EmptyInventoryException | OutOfTimeException | NotEnoughItemsException e) {
                        break;
                    }
                    updateDrone(drone);
                }
            }
            for (Drone drone : drones) {
                if (drone.isAvailable() && droneWarehouse.get(drone) != null && !drone.getCarriedProducts().isEmpty()) {
                    try {
                        getOrder(drone, orders);
                        if (droneOrder.get(drone) == null) {
                            continue;
                        }
                        int quantity = droneOrder.get(drone).getContents().get(droneProduct.get(drone));
                        if (quantity > drone.getCarriedProducts().get(droneProduct.get(drone))) {
                            quantity = drone.getCarriedProducts().get(droneProduct.get(drone));
                        }

                        drone.deliver(droneOrder.get(drone), droneProduct.get(drone), quantity, MAX_TURNS - i);
                        droneOrder.get(drone).getLoaded().removeContents(droneProduct.get(drone), quantity);
                        if (droneWarehouse.get(drone).getProducts().isEmpty()) {
                            warehouses.remove(droneWarehouse.get(drone));
                        }
                        if (droneOrder.get(drone).getLoaded().isEmpty()) {
                            orders.remove(droneOrder.get(drone));
                        }
                        if (hasOrder(drone, orders) && (drone.getWeight() - droneProduct.get(drone).getWeight()*quantity) == 0) {
                            droneWarehouse.remove(drone);
                        }
                        if (!hasOrder(drone, orders) && (drone.getWeight() - droneProduct.get(drone).getWeight()*quantity) == 0) {
                            droneProduct.remove(drone);
                            droneWarehouse.remove(drone);
                        }
                        if (!hasOrder(drone, orders)) {
                            droneProduct.remove(drone);
                        }
                        droneOrder.remove(drone);
                    } catch (EmptyInventoryException | OutOfTimeException | NotEnoughItemsException e) {
                        break;
                    }
                    updateDrone(drone);
                }
            }
            conditionalUpdate(drones);
        }
        return res.toString();
    }

    private void assignProduct(Drone drone) {
        for (Product p : productSet) {
            droneProduct.put(drone, p);
            productSet.remove(p);
            return;
        }
    }

    private void getWarehouse(Drone drone, List<Warehouse> warehouses) {
        Warehouse bestWarehouse = null;
        for (Warehouse warehouse : warehouses) {
            Product p = droneProduct.get(drone);
            if (warehouse.getProducts().containsKey(p)) {
                if (bestWarehouse != null) {
                    if (warehouse.getProducts().get(p) > bestWarehouse.getProducts().get(p)) {
                        bestWarehouse = warehouse;
                    }
                } else {
                    bestWarehouse = warehouse;
                }
            }
        }
        if (bestWarehouse != null) {
            droneWarehouse.put(drone, bestWarehouse);
        }
    }

    private void getOrder(Drone drone, List<Order> orders) {
        for (Order order : orders) {
            Product p = droneProduct.get(drone);
            if (order.getContents().containsKey(p)) {
                droneOrder.put(drone, order);
            }
        }
    }

    private boolean hasOrder(Drone drone, List<Order> orders) {
        for (Order order : orders) {
            Product p = droneProduct.get(drone);
            if (order.getLoaded().containsKey(p)) {
                return true;
            }
        }
        return false;
    }

    private void conditionalUpdate(List<Drone> drones) {
        for (Drone drone : drones) {
            if (!drone.isAvailable() && droneWarehouse.get(drone) == null) {
                updateDrone(drone);
            }
            if (!drone.isAvailable() && droneWarehouse != null) {
                updateDrone(drone);
            }
        }
    }

}
