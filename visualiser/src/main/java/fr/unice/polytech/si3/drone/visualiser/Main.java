package fr.unice.polytech.si3.drone.visualiser;

import fr.unice.polytech.si3.drone.exception.*;

import java.io.IOException;

/**
 * @author Guillaume Andre
 */
public class Main {

    public static void main(String[] args) throws BadWeightException, OutOfTimeException, IOException, EmptyInventoryException, SyntaxErrorException, NotEnoughItemsException {
        new Launcher().launch(args);
    }

}
