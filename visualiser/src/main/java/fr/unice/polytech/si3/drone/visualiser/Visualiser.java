package fr.unice.polytech.si3.drone.visualiser;

import fr.unice.polytech.si3.drone.*;
import fr.unice.polytech.si3.drone.exception.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Guillaume André
 * @version 1.0
 *          <p>
 *          Displays a simulation of a given strategy.
 */
public class Visualiser {

    private static final String LINE = "--------------------------------------------------------------------------------------------";

    private ActionParser ap;
    private Parser parser;
    private File context;
    private File actions;
    private List<Integer> ordersComplete;
    private Scanner sc;
    private boolean isOperator = false;
    private int customerID;
    private List<Drone> drones;
    private List<Warehouse> warehouses;
    private List<Order> orders;
    private List<Product> products;
    private List<ActionQueue> actionsToExecute;

    /**
     * Standard constructor.
     *
     * @param actions - the output from the scheduler
     * @param context - the context of the simulation
     * @throws FileNotFoundException - if the scheduler file was not found
     */
    public Visualiser(File actions, File context) throws FileNotFoundException {
        this.context = context;
        this.actions = actions;
        ap = new ActionParser(actions);
        parser = new Parser(context);
        sc = new Scanner(System.in);
    }

    /**
     * Launches the drone simulation.
     *
     * @throws EmptyInventoryException - if trying to unload a product not on a drone
     * @throws OutOfTimeException      - if trying to perform an action taking longer than the remaining amount of turns
     * @throws BadWeightException      - if trying to load the drone beyond maximum capacity
     * @throws IOException             - if failing to read the scheduler output
     * @throws NotEnoughItemsException - if failing to remove from an inventory
     */
    public void launch() throws EmptyInventoryException, OutOfTimeException, BadWeightException, IOException, SyntaxErrorException, NotEnoughItemsException {
        simulateAllOnce();
        parser.parse();
        drones = parser.getDrones();
        orders = parser.getOrders();
        for (int i = 0; i < orders.size(); i++) {
            orders.get(i).setTurnComplete(ordersComplete.get(i));
        }
        products = parser.getProducts();
        warehouses = parser.getWarehouses();
        actionsToExecute = ap.parse(drones);
        choosePOV();
        loopTurns();
    }

    /**
     * Simulates all actions once, to determine when each order will be completed.
     *
     * @throws SyntaxErrorException    - if the context file is malformed
     * @throws IOException             - if failing to read a file
     * @throws BadWeightException      - if trying to load the drone beyond maximum capacity
     * @throws EmptyInventoryException - if trying to unload an item absent from an inventory
     * @throws NotEnoughItemsException - if failing to remove from an inventory
     */
    private void simulateAllOnce() throws SyntaxErrorException, IOException, BadWeightException, OutOfTimeException, EmptyInventoryException, NotEnoughItemsException {
        parser.parse();
        drones = parser.getDrones();
        orders = parser.getOrders();
        ordersComplete = new ArrayList<>();
        products = parser.getProducts();
        warehouses = parser.getWarehouses();
        actionsToExecute = ap.parse(drones);
        ordersComplete = new ArrayList<>();
        for (Order o : orders) {
            ordersComplete.add(-1);
        }
        for (int i = 0; i < Parser.MAX_TURNS; i++) {
            executeTurn(Parser.MAX_TURNS - i);
            for (Order o : orders) {
                if (o.isEmpty()) {
                    if (ordersComplete.get(o.getId()) == -1) {
                        ordersComplete.set(o.getId(), i);
                    }
                }
            }
        }
        reset();
    }

    /**
     * Resets the parsers before starting the visualisation.
     *
     * @throws FileNotFoundException - if one of the file to parse was not found
     */
    private void reset() throws FileNotFoundException {
        parser = new Parser(context);
        ap = new ActionParser(actions);
    }

    /**
     * Asks the user for the point of view he wants for the visualization.
     */
    private void choosePOV() {
        System.out.println("Choose the point of view: Operator(o) or Customer(c0-" + (orders.size() - 1) + ").");
        String line;
        Pattern answer = Pattern.compile("o|c[0-9]+");
        Matcher matcher;
        while (true) {
            line = sc.nextLine();
            matcher = answer.matcher(line);
            if (matcher.find()) {
                break;
            } else {
                System.out.println("Invalid answer.");
            }
        }
        if (line.charAt(0) == 'o') {
            isOperator = true;
        } else {
            customerID = Integer.parseInt(line.substring(1));
        }
    }

    /**
     * Loops through all turns of the simulation.
     *
     * @throws EmptyInventoryException - if trying to unload a product not on a drone
     * @throws OutOfTimeException      - if trying to perform an action taking longer than the remaining number of turns
     * @throws BadWeightException      - if trying to load a drone beyond its maximum payload
     * @throws NotEnoughItemsException - if failing to remove from an inventory
     */
    private void loopTurns() throws EmptyInventoryException, OutOfTimeException, BadWeightException, NotEnoughItemsException {
        for (int i = 0; i < Parser.MAX_TURNS; i++) {
            if (isOperator) {
                System.out.println("Turn " + i + " :");
                if (!chooseViewOperator()) {
                    return;
                }
            } else {
                System.out.println("Turn " + i + " :");
                System.out.println(orders.get(customerID).toString());
                System.out.println(LINE);
                if (orders.get(customerID).isEmpty()) {
                    return;
                }
            }
            if (checkFinish()) {
                return;
            }
            executeTurn(Parser.MAX_TURNS - i);
        }
    }

    /**
     * As an operator, choose what aspect of the simulation to view on this turn.
     * Display what is requested.
     *
     * @return true to go to the next turn, false to end the simulation
     */
    private boolean chooseViewOperator() {
        String line;
        Pattern choice = Pattern.compile("[owdan]|end");
        Matcher matcher;
        while (true) {
            System.out.println("Pick what to view : Orders(o), Warehouses(w), Drones(d), All(a), or go to the next turn(n). \nEnd the simulation with \"end\".");
            while (true) {
                line = sc.nextLine();
                matcher = choice.matcher(line);
                if (matcher.find()) {
                    if (matcher.group().equals("end")) {
                        return false;
                    }
                    break;
                } else {
                    System.out.println("Invalid answer.");
                }
            }
            switch (line.charAt(0)) {
                case 'n':
                    return true;
                case 'o':
                    viewOrders();
                    break;
                case 'w':
                    viewWarehouses();
                    break;
                case 'd':
                    viewDrones();
                    break;
                case 'a':
                    viewWarehouses();
                    System.out.println(LINE);
                    viewDrones();
                    System.out.println(LINE);
                    viewOrders();
                    break;
            }
            System.out.println(LINE + "\n");
        }
    }

    /**
     * View the current status of all orders.
     */
    private void viewOrders() {
        System.out.println("Order status : ");
        for (Order order : orders) {
            System.out.println("Order #" + order.getId() + ": " + order.toString() + "\n");
        }
    }

    /**
     * View the status of all Warehouses.
     */
    private void viewWarehouses() {
        System.out.println("Warehouse Status : ");
        for (Warehouse warehouse : warehouses) {
            System.out.println("Warehouse #" + warehouse.getId() + ": " + warehouse.toString());
        }
    }

    /**
     * View the status of all drones.
     */
    private void viewDrones() {
        System.out.println("Drone status :");
        for (Drone drone : drones) {
            System.out.println("Drone #" + drone.getId() + ": " + drone.toString());
        }
    }

    /**
     * Executes a turn of the simulation. Performs all unload actions before the rest.
     *
     * @param turn - the number of turns left to simulate
     * @throws EmptyInventoryException - if trying to unload a product not in a drone
     * @throws OutOfTimeException      - if the drone does not have enough time to complete its action
     * @throws BadWeightException      - if a drone is overloaded
     * @throws NotEnoughItemsException - if failing to remove from an inventory
     */
    private void executeTurn(int turn) throws EmptyInventoryException, OutOfTimeException, BadWeightException, NotEnoughItemsException {
        for (ActionQueue queue : actionsToExecute) {
            if (queue.hasPriority()) {
                queue.doAction(warehouses, products, orders, turn);
            }
        }
        for (ActionQueue queue : actionsToExecute) {
            queue.doAction(warehouses, products, orders, turn);
        }
    }

    /**
     * Checks if all orders are complete.
     *
     * @return true if all orders are completed, false otherwise
     */
    private boolean checkFinish() {
        for (Order o : orders) {
            if (!o.isEmpty()) {
                return false;
            }
        }
        return true;
    }

}
