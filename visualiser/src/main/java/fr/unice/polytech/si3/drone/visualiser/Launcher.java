package fr.unice.polytech.si3.drone.visualiser;

import fr.unice.polytech.si3.drone.exception.*;

import java.io.File;
import java.io.IOException;

/**
 * @author Guillaume Andre
 *         <p>
 *         Initializes the visualiser.
 */
public class Launcher {

    /**
     * Launches the visualiser.
     *
     * @param args - the file paths of the scheduler output and the context file
     * @throws BadWeightException      - if trying to load beyond a drone maximum payload
     * @throws OutOfTimeException      - if attempting to do an action beyond the maximum turn amount
     * @throws SyntaxErrorException    - if the context file is malformed
     * @throws EmptyInventoryException - if trying to unload an item from an inventory where it isn't there
     * @throws IOException             - if failing to read one of the files
     */
    public void launch(String[] args) throws BadWeightException, OutOfTimeException, SyntaxErrorException, EmptyInventoryException, IOException, NotEnoughItemsException {
        if (args.length != 2) {
            System.out.println("Incorrect arguments: specify context and scheduler output.");
            System.exit(1);
        }
        Visualiser v = new Visualiser(new File(args[1]), new File(args[0]));
        v.launch();
    }

}
