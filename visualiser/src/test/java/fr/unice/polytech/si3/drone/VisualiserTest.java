package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.*;
import fr.unice.polytech.si3.drone.visualiser.Visualiser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;

/**
 * Test class for visualiser.
 */
public class VisualiserTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private Visualiser visualiser;
    private ByteArrayOutputStream bo;

    public File setUp(String content) throws IOException {
        File file = folder.newFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(content);
        bw.close();
        return file;
    }

    @Test
    public void emptyStrategy() throws IOException, EmptyInventoryException, OutOfTimeException, BadWeightException, SyntaxErrorException, NotEnoughItemsException {
        String content = "7 5 2 0 500 \n" +
                "3 \n" +
                "100 5 450 \n" +
                "1 \n" +
                "0 0 \n" +
                "5 1 0 \n" +
                "1 \n" +
                "1 1 \n" +
                "2 \n" +
                "2 0";
        File input = folder.newFile();
        File context = setUp(content);
        System.setOut(new PrintStream(new ByteArrayOutputStream()));
        ByteArrayInputStream by = new ByteArrayInputStream("o\nn\nn\nn\n".getBytes());
        System.setIn(by);
        Parser.MAX_TURNS = 3;
        visualiser = new Visualiser(input, context);
        visualiser.launch();
    }

    public String initStandard(String input) throws IOException, BadWeightException, OutOfTimeException, EmptyInventoryException, SyntaxErrorException, NotEnoughItemsException {
        String content = "7 7 2 50 500\n 3\n 100 5 450\n 2\n 0 0\n 5 1 1\n 5 5\n 0 10 2\n 3\n 1 1\n 2\n" +
                "2 0\n 3 3\n 1\n 0\n 5 6\n 1\n 2";
        String actions = "8 \n 0 L 0 0 1\n 0 D 0 0 1\n 0 L 0 2 1\n 0 D 0 2 1\n 0 L 0 0 1\n 0 D 1 0 1\n" +
                " 0 L 1 2 1\n 0 D 2 2 1";
        File context = setUp(content);
        File file = folder.newFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(actions);
        bw.close();
        bo = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bo));
        ByteArrayInputStream by = new ByteArrayInputStream(input.getBytes());
        System.setIn(by);
        visualiser = new Visualiser(file, context);
        visualiser.launch();
        return bo.toString();
    }

    @Test
    public void droneStatusTest() throws BadWeightException, OutOfTimeException, SyntaxErrorException, EmptyInventoryException, IOException, NotEnoughItemsException {
        String output = initStandard("o\nn\nd\nend\n");
        assert output.contains("Drone #0");
        assert output.contains("Product #0 : 1 left.");
        assert output.contains("Drone #1");
        assert output.contains("Empty");
    }

    @Test
    public void wareStatusTest() throws IOException, BadWeightException, OutOfTimeException, EmptyInventoryException, SyntaxErrorException, NotEnoughItemsException {
        String output = initStandard("o\nn\nw\nend\n");
        assert output.contains("Warehouse #0");
        assert output.contains("(0,0)");
        assert output.contains("Product #0");
        assert output.contains("4 available.");
        assert output.contains("Product #1");
        assert output.contains("1 available.");
    }

    @Test
    public void orderStatusTest() throws BadWeightException, OutOfTimeException, SyntaxErrorException, EmptyInventoryException, IOException, NotEnoughItemsException {
        String output = initStandard("o\nn\no\nend\n");
        assert output.contains("Order #0");
        assert output.contains("(1,1)");
        assert output.contains("Product #0");
        assert output.contains("Scheduled");
    }

    @Test
    public void customerTest() throws BadWeightException, OutOfTimeException, SyntaxErrorException, EmptyInventoryException, IOException, NotEnoughItemsException {
        String output = initStandard("c0\n");
        assert output.contains("Turn 6");
        assert output.contains("Delivering");
        assert output.contains("In progress");
    }


}