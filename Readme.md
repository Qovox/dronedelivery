# Team N - Drone delivery

## Auteurs

|||
|-------------------|-----------------------------------|
|Guillaume André    |guillaume.andre@etu.unice.fr		|
|Florian Bourniquel |florian.bourniquel@etu.unice.fr	|
|Jérémy Lara        |jeremy.lara@etu.unice.fr			|
|Jean-Adam Puskaric |jean-adam.puskaric@etu.unice.fr    |                       		|

## Présentation
Ce programme permet de planifier les actions d'un ensemble de drones pour livrer des produits récupérés à des entrepôts. Une fois ces actions crées, il est également capable de les visualiser au tour par tour ou d'évaluer l'efficacité de la stratégie utilisée.

Le programme est divisé en 3 modules :
* Le module "Scheduler" permet, à partir d'un contexte donné, de créer une planification des actions de tous les drones dans un fichier .out, et crée également un fichier map.csv contenant la localisation de l'ensemble des entrepôts et des commandes. Il est également possible de créer le résultat de l'application de toutes les stratégies disponibles pour un contexte donné.

* Le module "Visualiser" permet, à partir de la sortie du scheduler et du contexte original associé, de visualiser les actions de cette stratégie et l'état des drone, entrepôts et commandes au tout par tour.

* Enfin, le module "Benchmark" récupère les actions d'une stratégie et le contexte associé, puis calcule des statistiques sur chacune des livraisons de ce contexte et produit une page html contenant ces résultats. Il est également possible de comparer plusieurs stratégies correspondant au même contexte entre elles.

## Exécution
Chacun des modules du projet est éxécutable séparément par un maven exec : `mvn exec:java -Dexec.args="arguments"`

#### Scheduler
Le Scheduler prend en entrée un contexte .in donné et produit un fichier contenant les actions de la meilleure stratégie pour ce contexte, ou produit un ensemble de fichier contenant les actions de toutes les stratégies possibles pour ce contexte.
* Pour la meilleure stratégie : **[chemin du contexte - fichier .in]**
* Pour toutes les stratégies et le benchmark : **[chemin du contexte - fichier .in] -benchmark**
* Pour toutes les stratégies avec profiler :  **[chemin du contexte - fichier .in] -profiler**

L'option profiler permet de stopper le programme à chaque début et fin de stratégie en attendant un input utilisateur, ainsi on peut utiliser un profileur de code afin de regarder l'impact mémoire de la stratégie. les instructions pour utiliser un profileur de code avec notre projet sont à la fin de ce ReadMe

#### Visualiser
Le visualiseur prend en entrée un contexte .in donné et les actions d'une stratégie correspondant à ce contexte. Il prend les arguments suivants :
 * **[chemin du contexte - fichier .in] [chemin des actions - fichier.out produit par un Scheduler]**

#### Benchmark
Le benchmark peut utiliser 2 types d'arguments, selon que l'on calcule des statistiques sur une stratégie ou que l'en on compare plusieurs.Pour comparer plusieurs stratégies sur un même contexte, l'ensemble des fichiers .out correspondant à leurs actions doivent être placées dans un même dossier, avec le contexte **unique** associé à ces actions. Pour comparer leurs temps d'exécution, il faudra placer le fichier exectime.txt produit par le Scheduler dans ce même dossier.
* Pour une seule stratégie :  **[chemin du contexte - fichier .in] [chemin des actions - fichier .out produit par un Scheduler]**
* Pour plusieurs stratégies : **-d [chemin du dossier]**

#### Instructions pour le code profiler
Tout d'abord il faut installer un code profiler, dans cet exemple nous avons utilisé visualvm : https://visualvm.github.io/

Ensuite il faut mettre en place Maven afin de permettre une connexion JMX à la VM (sous windows il faut utiliser **set** mais sous linux le mot clé est **export**).

`set MAVEN_OPTS=-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9011 -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false`

Enfin il suffit de lancer le programme avec l'option -profile et de brancher le code profiler afin de voir en temps réel la consommation de données de nos stratégies.

Pour pouvoir réutiliser le benchmark ensuite il faut remettre à zéro le port qu'on a modifié:

`set MAVEN_OPTS=-Dcom.sun.management.jmxremote.port=0`
