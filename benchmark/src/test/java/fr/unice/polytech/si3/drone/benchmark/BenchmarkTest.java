package fr.unice.polytech.si3.drone.benchmark;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;


/**
 * Test class for Benchmark.
 */
public class BenchmarkTest {

    private final String TEST_CONTEXT = "7 7 2 50 500\n" +
            "3\n" +
            "100 5 450\n" +
            "2\n" +
            "0 0\n" +
            "5 1 1\n" +
            "5 5\n" +
            "0 10 2\n" +
            "3\n" +
            "1 1\n" +
            "2\n" +
            "2 0\n" +
            "3 3\n" +
            "1\n" +
            "0\n" +
            "5 6\n" +
            "1\n" +
            "2";

    private final String EXAMPLE_STRATEGY = "8 \n" +
            "0 L 0 0 1\n" +
            "0 D 0 0 1\n" +
            "0 L 0 2 1\n" +
            "0 D 0 2 1\n" +
            "0 L 0 0 1\n" +
            "0 D 1 0 1\n" +
            "0 L 1 2 1\n" +
            "0 D 2 2 1";

    @Rule
    public ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private File fileIn;
    private File fileOut;

    @Before
    public void SetUp() throws IOException {
        fileOut = folder.newFile("test.out");
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileOut));
        writer.write(EXAMPLE_STRATEGY);
        writer.close();
        fileIn = folder.newFile("test.in");
        BufferedWriter writer2 = new BufferedWriter(new FileWriter(fileIn));
        writer2.write(TEST_CONTEXT);
        writer2.close();
    }

    @Test
    public void launchBenchmark() throws Exception {
        Benchmark benchmark = new Benchmark(fileIn, fileOut, false);
        benchmark.launch();
        Map<Integer, Integer> score = new HashMap<>();
        score.put(0, 88);
        score.put(1, 74);
        score.put(2, 66);
        List<Integer> usedDrone;
        assertEquals(benchmark.getLastScoreOrders(), score);
        assertEquals(1, Integer.parseInt(benchmark.getBenchData().get(0).get(5)));
    }

    private File generateTestFolder() throws IOException {
        File directory = folder.newFolder();
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(directory, "context.in")));
        bw.write(TEST_CONTEXT);
        bw.close();
        bw = new BufferedWriter(new FileWriter(new File(directory, "test.out")));
        bw.write(EXAMPLE_STRATEGY);
        bw.close();
        bw = new BufferedWriter(new FileWriter(new File(directory, "test2.out")));
        bw.write("8 \n 0 L 0 0 1 \n 1 L 0 2 1 \n 0 D 0 0 1 \n 1 D 0 2 1 \n 0 L 0 0 1 \n 0 D 1 0 1 \n 1 L 1 2 1 \n 1 D 2 2 1");
        bw.close();
        return directory;
    }

    @Test
    public void compareStrategiesTest() throws Exception {
        Benchmark benchmark = new Benchmark(generateTestFolder(), null, true);
        benchmark.launch();
        List<List<String>> got = benchmark.getBenchData();
        List<String> firstStrategy = got.get(0);
        List<String> secondStrategy = got.get(1);
        assertEquals("test", firstStrategy.get(0));
        assertEquals("8", firstStrategy.get(1));
        assertEquals("0", firstStrategy.get(2));
        assertEquals("3", firstStrategy.get(3));
        assertEquals("1", firstStrategy.get(4));
        assertEquals("1", firstStrategy.get(5));
        assertEquals("Delivery 0", firstStrategy.get(6));
        assertEquals("88", firstStrategy.get(7));
        assertEquals("Delivery 1", firstStrategy.get(8));
        assertEquals("74", firstStrategy.get(9));
        assertEquals("Delivery 2", firstStrategy.get(10));
        assertEquals("66", firstStrategy.get(11));
        assertEquals("test2", secondStrategy.get(0));
        assertEquals("8", secondStrategy.get(1));
        assertEquals("0", secondStrategy.get(2));
        assertEquals("3", secondStrategy.get(3));
        assertEquals("0", secondStrategy.get(4));
        assertEquals("2", secondStrategy.get(5));
        assertEquals("Delivery 0", secondStrategy.get(6));
        assertEquals("96", secondStrategy.get(7));
        assertEquals("Delivery 1", secondStrategy.get(8));
        assertEquals("82", secondStrategy.get(9));
        assertEquals("Delivery 2", secondStrategy.get(10));
        assertEquals("82", secondStrategy.get(11));
    }

    @Test
    public void folderWithoutContext() throws IOException {
        File directory = folder.newFolder();
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(directory, "test.out")));
        bw.write(EXAMPLE_STRATEGY);
        bw.close();
        bw = new BufferedWriter(new FileWriter(new File(directory, "exectime.txt")));
        bw.write("0");
        bw.close();
        exit.expectSystemExitWithStatus(1);
        Benchmark benchmark = new Benchmark(directory, null, true);
    }

    @Test
    public void folderWithoutOutput() throws IOException {
        File directory = folder.newFolder();
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(directory, "context.in")));
        bw.write(TEST_CONTEXT);
        bw.close();
        exit.expectSystemExitWithStatus(1);
        Benchmark benchmark = new Benchmark(directory, null, true);
    }

    @Test
    public void multipleContext() throws IOException {
        File directory = folder.newFolder();
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(directory, "context.in")));
        bw.write(TEST_CONTEXT);
        bw.close();
        bw = new BufferedWriter(new FileWriter(new File(directory, "context2.in")));
        bw.write(TEST_CONTEXT);
        bw.close();
        exit.expectSystemExitWithStatus(1);
        Benchmark benchmark = new Benchmark(directory, null, true);
    }

    @Test
    public void notAFolder() throws IOException {
        File directory = folder.newFile();
        exit.expectSystemExitWithStatus(1);
        Benchmark benchmark = new Benchmark(directory, null, true);
    }

}