package fr.unice.polytech.si3.drone.benchmark;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Test class for WriteBenchmark.
 */
public class WriteBenchmarkTest {

    private File file;
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void writeBenchmark() throws Exception {
        Map<Integer,Integer> score = new HashMap<>();
        score.put(0,90);
        score.put(1,82);
        score.put(2,66);
        File simpleBenchmark = folder.newFile("simpleBenchmark.html");
        new WriteBenchmark().writeBenchmark(score,1,1,simpleBenchmark);
        assertEquals("<html>\n" +
                "<head>\n" +
                "  <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n" +
                "  <script type=\"text/javascript\">\n" +
                "\n" +
                "       google.charts.load('current', {packages: ['corechart', 'bar']});\n" +
                "      google.charts.setOnLoadCallback(Delivery);\n" +
                "      google.charts.setOnLoadCallback(Drone);\n" +
                "      function Delivery() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Delivery', 'Score'],\n" +
                "\t\t\t  ['Delivery 0', 90],\n" +
                "\t\t\t  ['Delivery 1', 82],\n" +
                "\t\t\t  ['Delivery 2', 66],\n" +
                "\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Score for each delivery',\n" +
                "                  subtitle: 'Total score : 238 \\n Average score : 79'}};\n" +
                "          var chart = new google.charts.Bar(document.getElementById('delivery_chart_div'));\n" +
                "          chart.draw(data, options);\n      }\n\n" +
                "      function Drone() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Type of drone', 'Number'],\n" +
                "\t\t\t  ['Used', 1],\n" +
                "\t\t\t  ['Unused', 1],\n" +
                "\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "                  title: 'Number of used and unused drone \\n Number of drone: 2'};\n" +
                "          var chart = new google.visualization.PieChart(document.getElementById('drone_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "  </script>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div id=\"delivery_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"drone_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "</body>\n" +
                "</html>", new Scanner(simpleBenchmark).useDelimiter("\\Z").next());
    }

    @Test
    public void writeChart() throws Exception {
        List<List<String>> list = new LinkedList<>();
        List<String> list1 = new LinkedList<>();
        list1.add("Strategy 1");
        list1.add("6");
        list1.add("30");
        list1.add("1");
        list1.add("0");
        list1.add("3");
        list1.add("Delivery 0");
        list1.add("90");
        List<String> list2 = new LinkedList<>();
        list2.add("Strategy 2");
        list2.add("7");
        list2.add("15");
        list2.add("1");
        list2.add("1");
        list2.add("2");
        list2.add("Delivery 0");
        list2.add("80");
        List<String> list3 = new LinkedList<>();
        list3.add("Strategy 3");
        list3.add("8");
        list3.add("27");
        list3.add("1");
        list3.add("2");
        list3.add("1");
        list3.add("Delivery 0");
        list3.add("70");
        List<String> list4 = new LinkedList<>();
        list4.add("Strategy 4");
        list4.add("2");
        list4.add("10");
        list4.add("1");
        list4.add("0");
        list4.add("3");
        list4.add("Delivery 0");
        list4.add("60");
        list.add(list1);
        list.add(list2);
        list.add(list3);
        list.add(list4);
        File html = folder.newFile("benchmark.html");
        new WriteBenchmark().writeChart(list,html);
        assertEquals("<html>\n" +
                "<head>\n" +
                "  <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n" +
                "  <script type=\"text/javascript\">\n" +
                "\n" +
                "       google.charts.load('current', {packages: ['corechart', 'bar']});\n" +
                "      google.charts.setOnLoadCallback(Strategy1);\n" +
                "      google.charts.setOnLoadCallback(DroneStrategy1);\n" +
                "      google.charts.setOnLoadCallback(Strategy2);\n" +
                "      google.charts.setOnLoadCallback(DroneStrategy2);\n" +
                "      google.charts.setOnLoadCallback(Strategy3);\n" +
                "      google.charts.setOnLoadCallback(DroneStrategy3);\n" +
                "      google.charts.setOnLoadCallback(Strategy4);\n" +
                "      google.charts.setOnLoadCallback(DroneStrategy4);\n" +
                "      google.charts.setOnLoadCallback(Delivery);\n" +
                "      google.charts.setOnLoadCallback(DeliveryAll);\n" +
                "      google.charts.setOnLoadCallback(Operation);\n" +
                "      google.charts.setOnLoadCallback(Metric);\n" +
                "      function Strategy1() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Delivery', 'Score'],\n" +
                "\t\t\t  ['Delivery 0', 90],\n" +
                "\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Score for each delivery, strategy : Strategy 1',\n" +
                "                  subtitle: 'Total score : 90    Average score : 90\\n Number of operations : 6'}};\n" +
                "          var chart = new google.charts.Bar(document.getElementById('strategy_1_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n" +
                "      function DroneStrategy1() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Type of drone', 'Number'],\n" +
                "\t\t\t  ['Used', 3],\n" +
                "\t\t\t  ['Unused', 0],\n" +
                "\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "                  title: 'Number of used and unused drone, strategy : Strategy 1\\n Number of drone: 3'};\n" +
                "          var chart = new google.visualization.PieChart(document.getElementById('drone_strategy_1_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "      function Strategy2() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Delivery', 'Score'],\n" +
                "\t\t\t  ['Delivery 0', 80],\n" +
                "\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Score for each delivery, strategy : Strategy 2',\n" +
                "                  subtitle: 'Total score : 80    Average score : 80\\n Number of operations : 7'}};\n" +
                "          var chart = new google.charts.Bar(document.getElementById('strategy_2_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n" +
                "      function DroneStrategy2() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Type of drone', 'Number'],\n" +
                "\t\t\t  ['Used', 2],\n" +
                "\t\t\t  ['Unused', 1],\n" +
                "\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "                  title: 'Number of used and unused drone, strategy : Strategy 2\\n Number of drone: 3'};\n" +
                "          var chart = new google.visualization.PieChart(document.getElementById('drone_strategy_2_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "      function Strategy3() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Delivery', 'Score'],\n" +
                "\t\t\t  ['Delivery 0', 70],\n" +
                "\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Score for each delivery, strategy : Strategy 3',\n" +
                "                  subtitle: 'Total score : 70    Average score : 70\\n Number of operations : 8'}};\n" +
                "          var chart = new google.charts.Bar(document.getElementById('strategy_3_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n" +
                "      function DroneStrategy3() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Type of drone', 'Number'],\n" +
                "\t\t\t  ['Used', 1],\n" +
                "\t\t\t  ['Unused', 2],\n" +
                "\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "                  title: 'Number of used and unused drone, strategy : Strategy 3\\n Number of drone: 3'};\n" +
                "          var chart = new google.visualization.PieChart(document.getElementById('drone_strategy_3_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "      function Strategy4() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Delivery', 'Score'],\n" +
                "\t\t\t  ['Delivery 0', 60],\n" +
                "\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Score for each delivery, strategy : Strategy 4',\n" +
                "                  subtitle: 'Total score : 60    Average score : 60\\n Number of operations : 2'}};\n" +
                "          var chart = new google.charts.Bar(document.getElementById('strategy_4_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n" +
                "      function DroneStrategy4() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Type of drone', 'Number'],\n" +
                "\t\t\t  ['Used', 3],\n" +
                "\t\t\t  ['Unused', 0],\n" +
                "\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "                  title: 'Number of used and unused drone, strategy : Strategy 4\\n Number of drone: 3'};\n" +
                "          var chart = new google.visualization.PieChart(document.getElementById('drone_strategy_4_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "      function Delivery() {\n" +
                "\n" +
                "         var data = new google.visualization.DataTable();\n" +
                "          data.addColumn('number', 'Delivery');\n" +
                "          data.addColumn('number', 'Strategy 1');\n" +
                "          data.addColumn('number', 'Strategy 2');\n" +
                "          data.addColumn('number', 'Strategy 3');\n" +
                "          data.addColumn('number', 'Strategy 4');\n" +
                "          data.addRows([\n" +
                "\t\t\t\t[{v:0, f: 'Delivery0'},90,80,70,60,],\n" +
                "          ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Score for each strategy'}};\n" +
                "          var chart = new google.charts.Bar(document.getElementById('delivery_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n" +
                "      function DeliveryAll() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Strategy', 'Total score'],\n" +
                "\t\t\t  ['Strategy 1', 90],\n" +
                "\t\t\t  ['Strategy 2', 80],\n" +
                "\t\t\t  ['Strategy 3', 70],\n" +
                "\t\t\t  ['Strategy 4', 60],\n" +
                "          ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Total score for each strategy'}};\n" +
                "          var chart = new google.charts.Bar(document.getElementById('delivery_score_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n" +
                "      function Operation() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Strategy', 'Number of operation'],\n" +
                "\t\t\t  ['Strategy 1', 6],\n" +
                "\t\t\t  ['Strategy 2', 7],\n" +
                "\t\t\t  ['Strategy 3', 8],\n" +
                "\t\t\t  ['Strategy 4', 2],\n" +
                "          ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Number of operations for each strategy'}};\n" +
                "          var chart = new google.charts.Bar(document.getElementById('operation_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n" +
                "      function Metric() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Strategy', 'Exec time'],\n" +
                "\t\t\t  ['Strategy 1', 30],\n" +
                "\t\t\t  ['Strategy 2', 15],\n" +
                "\t\t\t  ['Strategy 3', 27],\n" +
                "\t\t\t  ['Strategy 4', 10],\n" +
                "          ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Exec time for each strategy in ms'}};\n" +
                "          var chart = new google.charts.Bar(document.getElementById('metric_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n" +
                "  </script>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div id=\"strategy_1_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"drone_strategy_1_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"strategy_2_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"drone_strategy_2_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"strategy_3_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"drone_strategy_3_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"strategy_4_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"drone_strategy_4_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"delivery_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"delivery_score_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"operation_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"metric_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "</body>\n" +
                "</html>", new Scanner(html).useDelimiter("\\Z").next());
    }
}