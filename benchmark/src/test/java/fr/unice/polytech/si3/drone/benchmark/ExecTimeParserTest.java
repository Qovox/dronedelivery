package fr.unice.polytech.si3.drone.benchmark;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * @author Guillaume Andre
 *         <p>
 *         Test class for ExecTimeParser.
 */
public class ExecTimeParserTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private ExecTimeParser parser;

    @Test
    public void parseTest() throws IOException {
        File times = folder.newFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(times));
        bw.write("# Run progress: 75,00% complete, ETA 00:02:04\n" +
                "# Fork: 1 of 1\n" +
                "# Warmup Iteration   1: 0,018 ms/op\n" +
                "Iteration   1: 0,016 ms/op\n" +
                "\n" +
                "\n" +
                "Result \"WholeFleet\":\n" +
                "  0,016 ms/op\n" +
                "\n" +
                "\n" +
                "# Run complete. Total time: 00:06:16\n" +
                "\n" +
                "Benchmark                                 (arg)  Mode  Cnt       Score   Error  Units\n" +
                "MetricsBenchmark.OneDroneMaxCharge  context1.txt  avgt   5      ≈ 10⁻⁴          ms/op\n" +
                "MetricsBenchmark.OneDrone          context1.txt  avgt    5   184585,307          ms/op\n" +
                "MetricsBenchmark.OneProduct        context1.txt  avgt    5       0,015          ms/op\n" +
                "MetricsBenchmark.WholeFleet        context1.txt  avgt    5       0,016          ms/op");
        bw.close();
        parser = new ExecTimeParser(times);
        assertEquals(184585.307, parser.getExecTime("OneDrone"), 0.1);
        assertEquals(0.015, parser.getExecTime("OneProduct"), 0.1);
        assertEquals(0.016, parser.getExecTime("WholeFleet"), 0.1);
        assertEquals(0.0, parser.getExecTime("OneDroneMaxCharge"), 0.1);
    }

}
