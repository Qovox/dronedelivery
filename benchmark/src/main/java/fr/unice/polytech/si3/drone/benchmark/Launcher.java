package fr.unice.polytech.si3.drone.benchmark;

import fr.unice.polytech.si3.drone.exception.*;

import java.io.File;
import java.io.IOException;

/**
 * @author Guillaume Andre
 *         <p>
 *         Initializes the benchamrk and creates the data charts.
 */
public class Launcher {

    /**
     * Checks the arguments used. If correct, uses them to create a benchmark
     * of a single strategy, or a benchmark comparing multiple strategies.
     *
     *
     * @param args - the arguments of the program
     * @throws EmptyInventoryException - if trying to unload an item from an inventory not containing said item
     * @throws OutOfTimeException      - if trying to do an action that would take more time than the maximum amount of turns
     * @throws BadWeightException      - if trying to overload a drone
     * @throws IOException             - if failing to read the input file or the scheduler output file
     * @throws SyntaxErrorException    - if the context file is malformed
     * @throws NotEnoughItemsException - if trying to unload more items than available in an inventory
     */
    public void launchBenchmark(String[] args) throws IOException, BadWeightException, OutOfTimeException, NotEnoughItemsException, EmptyInventoryException, SyntaxErrorException {
        if (args.length != 2) {
            System.out.println("Incorrect arguments used. Valid arguments: \n  -\t[context file path]" +
                    " [scheduler output file path] OR\n  -\t-d [directory] where the directory contains multiple " +
                    "scheduler outputs from a same context also in the folder.");
            System.exit(1);
        }
        if (args[0].equals("-d")) {
            File directory = new File(args[1]);
            if (!directory.exists()) {
                System.out.println("Directory not found.");
                System.exit(1);
            }
            Benchmark benchmark = new Benchmark(directory, null, true);
            benchmark.launch();
            benchmark.writeChart();
        } else {
            File context = new File(args[0]);
            File actions = new File(args[1]);
            if (!context.exists()) {
                System.out.println("Context file not found.");
                System.exit(1);
            }
            if (!actions.exists()) {
                System.out.println("Scheduler output file not found.");
                System.exit(1);
            }
            Benchmark benchmark = new Benchmark(context, actions , false);
            benchmark.launch();
            benchmark.writeSimpleBenchmark();
        }
    }

}
