package fr.unice.polytech.si3.drone.benchmark;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Guillaume Andre
 *         <p>
 *         Extracts the avergae execution times for the strategies.
 */
public class ExecTimeParser {

    private File execTime;

    /**
     * Standard constructor.
     *
     *
     * @param execTimes - the file containing the execution time data
     */
    public ExecTimeParser(File execTimes) {
        this.execTime = execTimes;
    }

    /**
     * Extracts the exec time of the requested strategy.
     *
     * @param strategyName - the name of the strategy
     * @return a float containing the execution time in milliseconds
     * @throws IOException - if failing to read the file
     */
    public float getExecTime(String strategyName) throws IOException {
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(execTime), "UTF8"));
        while ((line = br.readLine()) != null) {
            Pattern name = Pattern.compile(strategyName + "\\s");
            Matcher matcher = name.matcher(line);
            if (line.startsWith("MetricsBenchmark") && matcher.find()) {
                line = line.replaceFirst("MetricsBenchmark.[\\S]+\\s", "");
                if (line.contains("?") || line.contains("≈")) {
                    return (float) 0;
                }
                Pattern dataGroup = Pattern.compile("\\s[\\S]+\\s");
                matcher = dataGroup.matcher(line);
                for (int i = 0; i < 3; i++) {
                    matcher.find();
                    line = matcher.replaceFirst("");
                    matcher = dataGroup.matcher(line);
                }
                Pattern number = Pattern.compile("[0-9-,]+");
                matcher = number.matcher(line);
                matcher.find();
                return Float.parseFloat(matcher.group().replaceFirst(",", "."));
            }
        }
        return (float) 0;
    }


}
