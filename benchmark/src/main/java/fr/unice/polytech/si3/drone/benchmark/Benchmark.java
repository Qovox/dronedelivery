package fr.unice.polytech.si3.drone.benchmark;

import fr.unice.polytech.si3.drone.*;
import fr.unice.polytech.si3.drone.exception.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Florian Bourniquel
 * @version 1.0
 *          <p>
 *          class to generate the data for WriteBenchmark
 */

public class Benchmark {

    private List<ActionParser> actionParsers;
    private File context;
    private Map<Integer, Integer> lastScoreOrders;
    private List<List<String>> benchData;
    private ExecTimeParser execParser;
    private boolean hasMultiple;
    private boolean foundExecTime = false;

    /**
     * Constructor for the benchmark.
     *
     * @param contextOrFolder    - a file containing either a folder of multiple inputs and outputs, or just the context
     * @param actions            - a file containing the actions of the strategy (if evaluating a single strategy)
     * @param multipleStrategies - whether we want to compare multiple startegies or evaluate a single one
     * @throws FileNotFoundException - if one of the specifies files was not found
     */
    public Benchmark(File contextOrFolder, File actions, boolean multipleStrategies) throws FileNotFoundException {
        hasMultiple = multipleStrategies;
        benchData = new ArrayList<>();
        actionParsers = new ArrayList<>();
        if (multipleStrategies) {
            setUpMultiple(contextOrFolder);
        } else {
            this.context = contextOrFolder;
            actionParsers.add(new ActionParser(actions));
            benchData.add(new ArrayList<>());
            benchData.get(benchData.size() - 1).add(actions.getName().substring(0, actions.getName().lastIndexOf(".")));
        }
    }

    public List<List<String>> getBenchData() {
        return benchData;
    }

    public Map<Integer, Integer> getLastScoreOrders() {
        return lastScoreOrders;
    }

    /**
     * Sets up this benchmark to handle multiple strategies.
     * All strategies scheduler output are located in the specified folder.
     * The context file should also be in this folder.
     * Stops execution if no context or strategy output was found.
     *
     * @param folder - the folder containing the files
     * @throws FileNotFoundException - if the folder was not found.
     */
    public void setUpMultiple(File folder) throws FileNotFoundException {
        File[] files = folder.listFiles();
        if (files == null) {
            System.out.println("Either enter the path of a folder containing multiple outputs" +
                    " from different strategies and a single context file," +
                    "or the path of a single context and then a single strategy.");
            System.exit(1);
        }
        for (File file : files) {
            if (file.getName().endsWith(".in")) {
                if (context != null) {
                    System.out.println("Multiple context files .in found in folder.");
                    System.exit(1);
                }
                context = file;
            } else if (file.getName().endsWith(".out")) {
                actionParsers.add(new ActionParser(file));
                benchData.add(new ArrayList<>());
                benchData.get(benchData.size() - 1).add(file.getName().substring(0, file.getName().lastIndexOf(".")));
            } else if (file.getName().equals("exectime.txt")) {
                execParser = new ExecTimeParser(file);
                foundExecTime = true;
            }
        }
        if (context == null) {
            System.out.println("Context file not found in specified directory.");
            System.exit(1);
        }
        if (actionParsers.isEmpty()) {
            System.out.println("No scheduler output for the given context was found in the specified directory.");
            System.exit(1);
        }
    }

    /**
     * Launches the simulation for all strategies.
     *
     * @throws EmptyInventoryException - if trying to unload an item from an inventory not containing said item
     * @throws OutOfTimeException      - if trying to do an action that would take more time than the maximum amount of turns
     * @throws BadWeightException      - if trying to overload a drone
     * @throws IOException             - if failing to read the input file or the scheduler output file
     * @throws SyntaxErrorException    - if the context file is malformed
     * @throws NotEnoughItemsException - if trying to unload more items than available in an inventory
     */
    public void launch() throws EmptyInventoryException, OutOfTimeException, BadWeightException, IOException, SyntaxErrorException, NotEnoughItemsException {
        for (int i = 0; i < actionParsers.size(); i++) {
            doStrategy(actionParsers.get(i), i);
        }
    }

    /**
     * Simulates all the actions of a strategy and computes the bench data.
     * Adds this data to the list.
     *
     * @param actionParser - the parser for the actions of this strategy
     * @param index        - the index of this strategy in the bench data list
     * @throws EmptyInventoryException - if trying to unload an item from an inventory not containing said item
     * @throws OutOfTimeException      - if trying to do an action that would take more time than the maximum amount of turns
     * @throws BadWeightException      - if trying to overload a drone
     * @throws IOException             - if failing to read the input file or the scheduler output file
     * @throws SyntaxErrorException    - if the context file is malformed
     * @throws NotEnoughItemsException - if trying to unload more items than available in an inventory
     */
    private void doStrategy(ActionParser actionParser, int index) throws EmptyInventoryException,
            OutOfTimeException, BadWeightException, NotEnoughItemsException, IOException, SyntaxErrorException {
        Parser parser = new Parser(context);
        parser.parse();
        List<ActionQueue> queues = actionParser.parse(parser.getDrones());
        int dronesUsed = 0;
        for (ActionQueue actionQueue : queues) {
            if (actionQueue.hasAction())
                dronesUsed++;
        }
        Map<Integer, Integer> scoreOrders = new HashMap<>();
        for (int i = 0; i < Parser.MAX_TURNS; i++) {
            executeTurn(Parser.MAX_TURNS - i, parser.getWarehouses(), parser.getProducts(), parser.getOrders(), queues);
            for (Order order : parser.getOrders()) {
                if (order.isEmpty()) {
                    scoreOrders.putIfAbsent(order.getId(), computeScore(i));
                }
            }
        }
        lastScoreOrders = scoreOrders;
        List<String> benchStrategy = benchData.get(index);
        benchStrategy.add(Integer.toString(actionParsers.get(index).getActionCount()));
        if (hasMultiple && foundExecTime) {
            benchStrategy.add(Float.toString(execParser.getExecTime(benchStrategy.get(0))));
        } else {
            benchStrategy.add("0");
        }
        benchStrategy.add(Integer.toString(parser.getOrders().size()));
        benchStrategy.add(Integer.toString(parser.getDrones().size() - dronesUsed));
        benchStrategy.add(Integer.toString(dronesUsed));
        for (Integer orderId : scoreOrders.keySet()) {
            benchStrategy.add("Delivery " + Integer.toString(orderId));
            benchStrategy.add(scoreOrders.get(orderId).toString());
        }
        for (Order o : parser.getOrders()) {
            if (scoreOrders.get(o.getId()) == null) {
                benchStrategy.add("Delivery " + o.getId());
                benchStrategy.add("0");
            }
        }
    }

    /**
     * Executes a turn for all the drones.
     *
     * @param turn       - the number of turns left
     * @param warehouses - the list of the warehouses of this simulation
     * @param products   - the list of products of this simulation
     * @param orders     - the list of orders of this simulation
     * @param queueList  - the list of all action queues of all the drones
     * @throws EmptyInventoryException - if trying to unload an item from an inventory not containing said item
     * @throws OutOfTimeException      - if trying to do an action that would take more time than the maximum amount of turns
     * @throws BadWeightException      - if trying to overload a drone
     * @throws NotEnoughItemsException - if trying to unload more items than available in an inventory
     */
    private void executeTurn(int turn, List<Warehouse> warehouses, List<Product> products, List<Order> orders, List<ActionQueue> queueList)
            throws EmptyInventoryException, OutOfTimeException, BadWeightException, NotEnoughItemsException {
        for (ActionQueue queue : queueList) {
            if (queue.hasPriority()) {
                queue.doAction(warehouses, products, orders, turn);
            }
        }
        for (ActionQueue queue : queueList) {
            queue.doAction(warehouses, products, orders, turn);
        }
    }

    /**
     * Evaluates the score for a given delivery.
     *
     * @param actualTurn - the turn the delivery was completed
     * @return the delivery score
     */
    private int computeScore(int actualTurn) {
        return (int) ((float) (Parser.MAX_TURNS - actualTurn) / Parser.MAX_TURNS * 100);
    }

    /**
     * Produces a benchmark file.
     *
     * @throws IOException - if failing to write the file
     */
    public void writeSimpleBenchmark() throws IOException {
        new WriteBenchmark().writeBenchmark(lastScoreOrders, Integer.parseInt(benchData.get(0).get(4)), Integer.parseInt(benchData.get(0).get(5)));
    }

    /**
     * Writes a chart comparing different strategies for the same context.
     *
     * @throws IOException - if failing to write the file
     */
    public void writeChart() throws IOException {
        new WriteBenchmark().writeChart(benchData);
    }

}
