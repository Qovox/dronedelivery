package fr.unice.polytech.si3.drone.benchmark;


import fr.unice.polytech.si3.drone.exception.*;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, BadWeightException, OutOfTimeException, EmptyInventoryException, SyntaxErrorException, NotEnoughItemsException {
        new Launcher().launchBenchmark(args);
    }

}
