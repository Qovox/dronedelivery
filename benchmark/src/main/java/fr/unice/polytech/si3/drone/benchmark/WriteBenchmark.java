package fr.unice.polytech.si3.drone.benchmark;


import java.io.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Florian Bourniquel
 * @version 3.0
 *          <p>
 *          class to generate the benchmark.html
 */

public class WriteBenchmark {

    /**
     * Writes the benchmark file.
     *
     * @param scoreOrders         - a map of orders id and delivery scores
     * @param numberOfUnusedDrone - the amount of drones left unused
     * @param numberOfUsedDrone   - the amount of drones used
     * @param file                - the file the content will be written to
     * @throws IOException - if failing to write the file
     */
    public void writeBenchmark(Map<Integer, Integer> scoreOrders, int numberOfUnusedDrone, int numberOfUsedDrone, File file) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        int totalScore = 0;
        int numberDelivery = 0;
        bw.write("<html>\n" +
                "<head>\n" +
                "  <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n" +
                "  <script type=\"text/javascript\">\n" +
                "\n" +
                "       google.charts.load('current', {packages: ['corechart', 'bar']});\n" +
                "      google.charts.setOnLoadCallback(Delivery);\n" +
                "      google.charts.setOnLoadCallback(Drone);\n" +
                "      function Delivery() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Delivery', 'Score'],\n");
        for (Map.Entry<Integer, Integer> entry : scoreOrders.entrySet()) {
            int key = entry.getKey();
            numberDelivery++;
            int value = entry.getValue();
            totalScore = totalScore + value;
            bw.write("\t\t\t  ['Delivery " + key + "', " + value + "],\n");
        }

        bw.write("\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Score for each delivery',\n" +
                "                  subtitle: 'Total score : " + totalScore + " \\n Average score : " + totalScore/numberDelivery + "'}};\n" +
                "          var chart = new google.charts.Bar(document.getElementById('delivery_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n\n");

        bw.write("      function Drone() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Type of drone', 'Number'],\n" +
                "\t\t\t  ['Used', " + numberOfUsedDrone + "],\n" +
                "\t\t\t  ['Unused', " + numberOfUnusedDrone + "],\n" +
                "\t\t  ]);\n" +
                "\n" +
                "          var options = {\n" +
                "                  title: 'Number of used and unused drone \\n Number of drone: " + (numberOfUnusedDrone+numberOfUsedDrone) + "'};\n" +
                "          var chart = new google.visualization.PieChart(document.getElementById('drone_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n");


        bw.write("  </script>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div id=\"delivery_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"drone_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "</body>\n" +
                "</html>");

        bw.close();
    }

    /**
     * Writes the benchmark file.
     *
     * @param scoreOrders         - a map of orders id and delivery scores
     * @param numberOfUnusedDrone - the amount of drones left unused
     * @param numberOfUsedDrone   - the amount of drones used
     * @throws IOException - if failing to write the file
     */
    public void writeBenchmark(Map<Integer, Integer> scoreOrders, int numberOfUnusedDrone, int numberOfUsedDrone) throws IOException {
        writeBenchmark(scoreOrders, numberOfUnusedDrone, numberOfUsedDrone, new File("./benchmark.html"));
    }

    /**
     * Writes a benchmark file comparing different strategies.
     *
     *
     * @param html      - the output file
     * @throws IOException - if failing to write the file
     */
    public void writeChart(List<List<String>> data, File html) throws IOException {
        Map<String,Integer> scoreTotal = new HashMap<>();
        int maxNumberDelivery = 0;
        BufferedWriter bw = new BufferedWriter(new FileWriter(html));
        bw.write("<html>\n" +
                "<head>\n" +
                "  <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n" +
                "  <script type=\"text/javascript\">\n" +
                "\n" +
                "       google.charts.load('current', {packages: ['corechart', 'bar']});" +
                "\n");
        for (List<String> list:data) {
            bw.write("      google.charts.setOnLoadCallback(" + list.get(0).replaceAll("\\s+","") + ");\n");
            bw.write("      google.charts.setOnLoadCallback(Drone" + list.get(0).replaceAll("\\s+","") + ");\n");
            if (Integer.parseInt(list.get(3)) > maxNumberDelivery)
                maxNumberDelivery = Integer.parseInt(list.get(3));
        }

        bw.write("      google.charts.setOnLoadCallback(Delivery);\n");
        bw.write("      google.charts.setOnLoadCallback(DeliveryAll);\n");
        bw.write("      google.charts.setOnLoadCallback(Operation);\n");
        bw.write("      google.charts.setOnLoadCallback(Metric);\n");

        for (List<String> list:data) {
            int totalScore = 0;
            bw.write( "      function " + list.get(0).replaceAll("\\s+","") + "() {\n" +
                    "\n" +
                    "          var data = google.visualization.arrayToDataTable([\n" +
                    "              ['Delivery', 'Score'],\n");
            for (int i = 0; i < Integer.parseInt(list.get(3)); i++) {
                int position = list.indexOf("Delivery " + i);
                bw.write("\t\t\t  ['" + list.get(position) + "', " + list.get(position+1) + "],\n");
                totalScore = totalScore + Integer.parseInt(list.get(position+1));
            }
            scoreTotal.put(list.get(0),totalScore);
            int averageScore;
            if (totalScore != 0 && Integer.parseInt(list.get(3)) != 0)
                averageScore = totalScore/Integer.parseInt(list.get(3));
            else
                averageScore = 0;

            bw.write("\t\t  ]);\n" +
                    "\n" +
                    "          var options = {\n" +
                    "              chart: {\n" +
                    "                  title: 'Score for each delivery, strategy : " + list.get(0) + "',\n" +
                    "                  subtitle: 'Total score : " + totalScore + "    Average score : " + averageScore +
                    "\\n Number of operations : " + list.get(1) + "'}};\n" +
                    "          var chart = new google.charts.Bar(document.getElementById('" + list.get(0).replaceAll("\\s+","_").toLowerCase() + "_chart_div'));\n" +
                    "          chart.draw(data, options);\n" +
                    "      }\n\n");

            bw.write("      function Drone" + list.get(0).replaceAll("\\s+","") + "() {\n" +
                    "\n" +
                    "          var data = google.visualization.arrayToDataTable([\n" +
                    "              ['Type of drone', 'Number'],\n" +
                    "\t\t\t  ['Used', " + list.get(5) + "],\n" +
                    "\t\t\t  ['Unused', " + list.get(4) + "],\n" +
                    "\t\t  ]);\n" +
                    "\n" +
                    "          var options = {\n" +
                    "                  title: 'Number of used and unused drone, strategy : " + list.get(0) +  "\\n Number of drone: " +
                    (Integer.parseInt(list.get(4))+ Integer.parseInt(list.get(5))) + "'};\n" +
                    "          var chart = new google.visualization.PieChart(document.getElementById('drone_" + list.get(0).replaceAll("\\s+","_").toLowerCase() + "_chart_div'));\n" +
                    "          chart.draw(data, options);\n" +
                    "      }\n");
        }


        bw.write("      function Delivery() {\n" +
                "\n" +
                "         var data = new google.visualization.DataTable();\n" +
                "          data.addColumn('number', 'Delivery');\n");
        for (List<String> list : data) {
            bw.write("          data.addColumn('number', '" + list.get(0) + "');\n");
        }
        bw.write("          data.addRows([\n");

        for (int i = 0; i < maxNumberDelivery; i++) {
            bw.write("\t\t\t\t[{v:" + i + ", f: 'Delivery" + i + "'},");
            for (List<String> list : data) {
                int position = list.indexOf("Delivery " + i);
                if (position > -1)
                    bw.write(list.get(position + 1) + ",");
                else
                    bw.write("0,");
            }
            bw.write("],\n");
        }
        bw.write("          ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Score for each strategy'}};" +
                "\n" +
                "          var chart = new google.charts.Bar(document.getElementById('delivery_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n");

        bw.write("      function DeliveryAll() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Strategy', 'Total score'],\n");
        for (List<String> list : data) {
            bw.write("\t\t\t  ['" + list.get(0) + "', " + scoreTotal.get(list.get(0)) + "],\n");
        }


        bw.write("          ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Total score for each strategy'}};" +
                "\n" +
                "          var chart = new google.charts.Bar(document.getElementById('delivery_score_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n");

        bw.write("      function Operation() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Strategy', 'Number of operation'],\n");
        for (List<String> list : data) {
            bw.write("\t\t\t  ['" + list.get(0) + "', " + list.get(1) + "],\n");
        }


        bw.write("          ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Number of operations for each strategy'}};" +
                "\n" +
                "          var chart = new google.charts.Bar(document.getElementById('operation_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n");

        bw.write("      function Metric() {\n" +
                "\n" +
                "          var data = google.visualization.arrayToDataTable([\n" +
                "              ['Strategy', 'Exec time'],\n");
        for (List<String> list : data) {
            bw.write("\t\t\t  ['" + list.get(0) + "', " + list.get(2) + "],\n");
        }


        bw.write("          ]);\n" +
                "\n" +
                "          var options = {\n" +
                "              chart: {\n" +
                "                  title: 'Exec time for each strategy in ms'}};" +
                "\n" +
                "          var chart = new google.charts.Bar(document.getElementById('metric_chart_div'));\n" +
                "          chart.draw(data, options);\n" +
                "      }\n" +
                "\n");

        bw.write("  </script>\n" +
                "</head>\n" +
                "<body>\n");
        for (List<String> list : data) {
            bw.write("<div id=\""+  list.get(0).replaceAll("\\s+","_").toLowerCase() +"_chart_div\" style=\"border: 1px solid #ccc\"></div>\n");
            bw.write("<div id=\"drone_"+  list.get(0).replaceAll("\\s+","_").toLowerCase() +"_chart_div\" style=\"border: 1px solid #ccc\"></div>\n");
        }


        bw.write("<div id=\"delivery_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"delivery_score_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"operation_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "<div id=\"metric_chart_div\" style=\"border: 1px solid #ccc\"></div>\n" +
                "</body>\n" +
                "</html>\n");
        bw.close();
    }

    /**
     * Writes a benchmark file comparing different strategies.
     *
     * @throws IOException - if failing to write the file
     */
    public void writeChart(List<List<String>> data) throws IOException {
        writeChart(data,new File("./benchmark.html"));
    }

}
