package fr.unice.polytech.si3.drone;


import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to bind a list of product and the customer who ordered it
 *
 * @version 1.0
 */
public class Order {

    private int turnComplete = -1;
    private int id;
    private Inventory contents;
    private Inventory loaded;
    private Coord destination;
    private List<Drone> affectedDrones;

    /**
     * Standard constructor, initialize parameters.
     *
     * @param contents    - a map of the contents of the order and the amount of each product
     * @param destination - the coordinates of the destination
     * @param id          - the order's id
     */
    public Order(Map<Product, Integer> contents, Coord destination, int id) {
        this.id = id;
        Map<Product, Integer> newContents = new HashMap<>();
        newContents.putAll(contents);
        this.contents = new Inventory(newContents);
        Map<Product, Integer> newContents2 = new HashMap<>();
        newContents2.putAll(contents);
        loaded = new Inventory(newContents2);
        this.destination = destination;
        affectedDrones = new ArrayList<>();
    }

    /**
     * Removes a product from this order inventory once delivered.
     *
     * @param p - the product
     * @param q - amount to remove
     * @throws NotEnoughItemsException - if failing to remove from an inventory
     */
    public void removeProduct(Product p, int q) throws NotEnoughItemsException {
        contents.removeContents(p, q);
    }

    /**
     * Sets a product as scheduled in this order inventory.
     * This means a drone will deliver the selected product.
     *
     * @param p - the product
     * @param q - amount to remove
     * @throws NotEnoughItemsException - if failing to remove from an inventory
     */
    public void removeProductLoaded(Product p, int q) throws NotEnoughItemsException {
        loaded.removeContents(p, q);
    }

    /**
     * Affects a drone to this order.
     *
     * @param d - the drone to affect
     */
    public void addAffectedDrone(Drone d) {
        affectedDrones.add(d);
    }

    /**
     * Unmap a drone to this order.
     *
     * @param d _ the drone to unmap
     */
    public void unAffect(Drone d) {
        affectedDrones.remove(d);
    }

    /**
     * Checks if this order was completed (empty inventory).
     *
     * @return true if completed, false otherwise
     */
    public boolean isEmpty() {
        return contents.isEmpty();
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            affectedDrones = new ArrayList<>();
        }
        String result = "Located at (" + destination.getX() + "," + destination.getY() + "). \n";
        if (!contents.isEmpty()) {
            result += "Inventory: \n " + contents.toString();
        }
        result += "Status: ";
        if (contents.isEmpty() && affectedDrones.isEmpty()) {
            result += "Completed. ";
        } else if (affectedDrones.isEmpty()) {
            result += "Scheduled. ";
        } else {
            result += "In progress. ";
        }
        result += "Affected drones: ";
        if (affectedDrones.isEmpty()) {
            result += "None";
        } else {
            result += "\n";
            for (Drone d : affectedDrones) {
                result += "\tDrone #" + d.getId() + ": Status: " + d.toString();
            }
        }
        if (turnComplete != 0) {
            result += "\nWill be completed on turn " + turnComplete + ".";
        }
        return result;
    }

    public Inventory getContents() {
        return contents;
    }

    public Inventory getLoaded() {
        return loaded;
    }

    public Coord getDestination() {
        return destination;
    }

    public int getId() {
        return id;
    }

    public void setTurnComplete(int turn) {
        this.turnComplete = turn + 1;
    }

}
