package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Guillaume Andre, Jeremy Lara
 * @version 1.2
 *          <p>
 *          This class represents a warehouse. A warehouse can contains several items
 *          of different types and is located by its coordinates.
 */
public class Warehouse {

    private int id;
    private Inventory products;
    private Inventory loaded;
    private Coord location;

    /**
     * Constructor of Warehouse class
     *
     * @param id       the id of the warehouse
     * @param products the products contained in the warehouse
     * @param location the location of the warehouse
     */
    public Warehouse(int id, Map<Product, Integer> products, Coord location) {
        this.id = id;
        Map<Product, Integer> newContents = new HashMap<>();
        newContents.putAll(products);
        this.products = new Inventory(newContents);
        Map<Product, Integer> newContents2 = new HashMap<>();
        newContents2.putAll(products);
        loaded = new Inventory(newContents2);
        this.location = location;
    }

    /**
     * Remove an item from the warehouse
     *
     * @param product the product to remove from the warehouse inventory
     * @param q       the quantity of the product to add
     * @throws EmptyInventoryException triggered if the warehouse's inventory is empty
     * @throws NotEnoughItemsException - if failing to remove from an inventory
     */
    public void remove(Product product, int q) throws EmptyInventoryException, NotEnoughItemsException {
        products.removeContents(product, q);
    }

    /**
     * Sets a product as scheduled in this order inventory.
     * This means a drone will deliver the selected product.
     *
     * @param p - the product
     * @param q - amount to remove
     * @throws NotEnoughItemsException - if failing to remove from an inventory
     */
    public void removeProductLoaded(Product p, int q) throws NotEnoughItemsException {
        loaded.removeContents(p, q);
    }

    /**
     * Add an item to the warehouse
     *
     * @param product the product to remove from the warehouse inventory
     * @param q       the amount of product to add
     */
    public void add(Product product, int q) {
        if (products.containsKey(product)) {
            products.put(product, products.get(product) + q);
        } else products.put(product, q);
    }

    @Override
    public String toString() {
        String result = "Warehouse #" + id + ", located at (" + location.getX() + "," + location.getY() + "), Inventory : \n";
        result += products.toString().replaceAll("left to deliver.", "available.");
        return result;
    }

    public int getId() {
        return id;
    }

    public Coord getLocation() {
        return location;
    }

    public Inventory getProducts() {
        return products;
    }

    public Inventory getLoaded() {
        return loaded;
    }

}
