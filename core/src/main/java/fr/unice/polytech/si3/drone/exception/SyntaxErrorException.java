package fr.unice.polytech.si3.drone.exception;

/**
 * @author Guillaume Andre
 * @version 1.0
 *
 * Exception thrown when the input file is incorrect.
 */
public class SyntaxErrorException extends Exception{

    /**
     * Standard constructor
     * @param message - details about the syntax error
     */
    public SyntaxErrorException(String message) {
        super("Error while parsing input file: \n"+message);
        System.out.println(getMessage());
        System.exit(2);
    }

}
