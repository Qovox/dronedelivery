package fr.unice.polytech.si3.drone;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Guillaume Andre
 *         <p>
 *         Parses the action log made with the scheduler.
 */
public class ActionParser {

    private BufferedReader br;
    private Pattern number = Pattern.compile("[0-9]+");
    private int actionCount;

    /**
     * Standard constructor.
     *
     * @param input - the output of the scheduler
     * @throws FileNotFoundException - if the output of the scheduler was not found
     */
    public ActionParser(File input) throws FileNotFoundException {
        br = new BufferedReader(new FileReader(input));
    }

    public int getActionCount() {
        return actionCount;
    }

    /**
     * Parses the actions in the scheduler log and translates it to their equivalent actionQueues.
     *
     * @param drones - the list of drones for this simulation
     * @return a list of corresponding actionQueues
     * @throws IOException - if failing to read the file
     */
    public List<ActionQueue> parse(List<Drone> drones) throws IOException {
        List<ActionQueue> queues = new ArrayList<>();
        for (Drone d : drones) {
            queues.add(new ActionQueue(d));
        }
        String line;
        Matcher matcher;
        if ((line = br.readLine()) != null) {
            matcher = number.matcher(line);
            if (!matcher.find()) {
                System.out.println("Could not find the total number of actions in a scheduler output file.");
                System.exit(1);
            }
            actionCount = Integer.parseInt(matcher.group());
            line = br.readLine();
            while (line != null) {
                matcher = number.matcher(line);
                matcher.find();
                int droneId = Integer.parseInt(matcher.group());
                line = matcher.replaceFirst("");
                queues.get(droneId).addAction(line.substring(1));
                line = br.readLine();
            }
        }
        return queues;
    }

}
