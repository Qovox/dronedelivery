package fr.unice.polytech.si3.drone.exception;

/**
 * @author Jeremy Lara
 * @version 1.0
 *          <p>
 *          Thrown when the inventory of the drone is empty and
 *          the action unload or deliver is called on this drone
 */
public class NotEnoughItemsException extends Exception {

    public NotEnoughItemsException() {
        super("Incorrect command, trying to remove too many item from an inventory");
    }

}
