package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Guillaume Andre
 *         <p>
 *         A queue of actions for a drone to execute during a simulation.
 */
public class ActionQueue {

    private List<String> actions;
    private boolean priority;
    private Drone drone;
    private boolean skip = false;

    /**
     * Standard constructor.
     */
    public ActionQueue(Drone drone) {
        actions = new ArrayList<>();
        this.drone = drone;
    }

    /**
     * Indicates whether this drone will be used for the simulation.
     * A drone is used if it has an action that is different from wait.
     */
    public boolean hasAction() {
        for (String s : actions) {
            if (s.contains("D") || s.contains("U") || s.contains("L")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a new action to this drone queue.
     *
     * @param s - the new action
     */
    public void addAction(String s) {
        actions.add(s);
    }

    /**
     * Checks if this drone action should be done before others.
     *
     * @return true if the drone will perform an unload on this turn, false otherwise
     */
    public boolean hasPriority() {
        return drone.isAvailable() && !actions.isEmpty() && actions.get(0).contains("U");
    }

    /**
     * Simulates a turn for the drone.
     *
     * @param warehouses - the list of all warehouses for this simulation
     * @param products   - the list of all products for this simulation
     * @param orders     - the list of all orders for this simulation
     * @param turn       - the amount of turns left for this simulation
     * @throws EmptyInventoryException - if trying to unload a product not on a drone
     * @throws BadWeightException      - if trying to load a drone beyond its maximum payload
     * @throws NotEnoughItemsException - if failing to remove from an inventory
     */
    public void doAction(List<Warehouse> warehouses, List<Product> products, List<Order> orders, int turn)
            throws EmptyInventoryException, BadWeightException, NotEnoughItemsException {
        try {
            if (skip) {
                skip = false;
                return;
            }
            if (!drone.isAvailable()) {
                drone.update();
                return;
            }
            if (!actions.isEmpty()) {
                String action = actions.get(0);
                Pattern number = Pattern.compile("[0-9]+");
                Matcher matcher = number.matcher(action);
                matcher.find();
                if (action.contains("U") || action.contains("L") || action.contains("D")) {
                    int warehouseOrderId = Integer.parseInt(matcher.group());
                    matcher.find();
                    int productId = Integer.parseInt(matcher.group());
                    matcher.find();
                    int amount = Integer.parseInt(matcher.group());
                    if (action.contains("U")) {
                        skip = true;
                        drone.unload(warehouses.get(warehouseOrderId), products.get(productId), amount, turn);
                    } else if (action.contains("L")) {
                        drone.load(warehouses.get(warehouseOrderId), products.get(productId), amount, turn);
                    } else {
                        drone.deliver(orders.get(warehouseOrderId), products.get(productId), amount, turn);
                    }
                } else if (action.contains("W")) {
                    int waitTime = Integer.parseInt(matcher.group());
                    drone.wait(waitTime, turn);
                }
                drone.update();
                actions.remove(0);
            } else {
                drone.resetString();
            }
        } catch (OutOfTimeException e) {
            //
        }
    }

    public List<String> getActions() {
        return actions;
    }

    public Drone getDrone() {
        return drone;
    }

}
