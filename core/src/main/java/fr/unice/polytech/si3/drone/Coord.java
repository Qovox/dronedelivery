package fr.unice.polytech.si3.drone;

/**
 * class to store coordinates and distance
 *
 * @version 1.0
 */
public class Coord {

    private int x;
    private int y;

    /**
     * Standard constructor for coordinates.
     *
     * @param x - the abscissa
     * @param y - the ordinate
     */
    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Default constructor, initializes to 0,0.
     */
    public Coord() {
        this.x = 0;
        this.y = 0;
    }

    @Override
    public String toString() {
        return x + " " + y;
    }

    /**
     * Euclidean distance between 2 coordinates.
     *
     * @param c - the other coordinate
     * @return the distance between this coord and the coord c
     */
    public int distance(Coord c) {
        return (int) Math.ceil(Math.sqrt(Math.pow(this.x - c.getX(), 2) + Math.pow(this.y - c.getY(), 2)));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coord coord = (Coord) o;

        return x == coord.x && y == coord.y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
