package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;

import java.util.Map;

/**
 * @author Guillaume Andre, Jeremy Lara
 * @version 1.3
 *          <p>
 *          This class represents a drone. A drone has a carry weight limit.
 *          It can carry several items but the sum of the weights of items has
 *          to be less or equal to the weight limit. A drone is located with
 *          its coordinates and has an identifier.
 */
public class Drone {

    private int id;
    private Inventory carriedProducts;
    private Coord location;
    private int available;
    private int currentActionTotal = 0;
    private String currentAction;
    private Warehouse warehouse;
    private Product product;
    private int itemNb;
    private Order order;

    /**
     * Constructor of Drone class
     *
     * @param id              the identifier of the drone
     * @param carriedProducts the list of products carried by the drone
     * @param location        the drone's location
     */
    public Drone(int id, Map<Product, Integer> carriedProducts, Coord location) {
        this.id = id;
        this.carriedProducts = new Inventory(carriedProducts);
        this.location = location;
        available = 0;
        currentAction = "Awaiting orders.";
    }

    /**
     * Load action. Moves the specified number of items of the
     * specified product type from a warehouse to the drone's inventory.
     *
     * @param wareHouse   the warehouse from which items are loaded
     * @param productType the product type to load
     * @param itemNb      the number of items of the product type to be loaded
     * @throws BadWeightException      triggered if the drone can't carry more items
     * @throws EmptyInventoryException triggered if the warehouse from which the drone wants to load is empty
     * @throws OutOfTimeException      triggered if the drone id too far away from the place where to load
     */
    public void load(Warehouse wareHouse, Product productType, int itemNb, int turnsLeft)
            throws BadWeightException, EmptyInventoryException, OutOfTimeException {
        if (location.distance(wareHouse.getLocation()) > turnsLeft) {
            throw new OutOfTimeException(location.distance(wareHouse.getLocation()), turnsLeft);
        } else {
            available = location.distance(wareHouse.getLocation());
            location = wareHouse.getLocation();
            currentAction = "Loading " + itemNb + " product(s) #" + productType.getId() + ": ";
            currentActionTotal = available;
            this.warehouse = wareHouse;
            this.product = productType;
            this.itemNb = itemNb;
        }
    }

    /**
     * Moves the specified number of items of the specified product type
     * from drone's inventory to a warehouse.
     *
     * @param wareHouse   the warehouse from which items are unloaded
     * @param productType the product type to load
     * @param itemNb      the number of items of the product type to be unloaded
     * @throws EmptyInventoryException triggered if the drone doesn't carry any item
     * @throws OutOfTimeException      triggered if the drone is too far away from the warehouse where
     *                                 the drone has to unload
     */
    public void unload(Warehouse wareHouse, Product productType, int itemNb, int turnsLeft)
            throws EmptyInventoryException, OutOfTimeException {
        if (location.distance(wareHouse.getLocation()) > turnsLeft) {
            throw new OutOfTimeException(location.distance(order.getDestination()), turnsLeft);
        } else {
            available = location.distance(wareHouse.getLocation());
            location = wareHouse.getLocation();
            currentAction = "Unloading " + itemNb + " product(s) #" + productType.getId() + ": ";
            currentActionTotal = available;
            warehouse = wareHouse;
            product = productType;
            this.itemNb = itemNb;
        }
    }

    /**
     * Delivers the specified number of items of the specified product type
     * to a customer
     *
     * @param order       the order of the items
     * @param productType the product type to deliver
     * @param itemNb      the number of items of the product type to be delivered
     * @throws EmptyInventoryException triggered if the drone is empty
     * @throws OutOfTimeException      triggered if the drone is too far away from the place
     *                                 it has to deliver items
     */
    public void deliver(Order order, Product productType, int itemNb, int turnsLeft)
            throws EmptyInventoryException, OutOfTimeException {
        order.addAffectedDrone(this);
        if (location.distance(order.getDestination()) > turnsLeft) {
            throw new OutOfTimeException(location.distance(order.getDestination()), turnsLeft);
        } else {
            available = location.distance(order.getDestination());
            location = order.getDestination();
            currentActionTotal = available;
            currentAction = "Delivering " + itemNb + " product(s) #" + productType.getId() + " for order #" + order.getId() + " : ";
            this.order = order;
            this.product = productType;
            this.itemNb = itemNb;
        }
    }

    /**
     * Waits the specified number of turns in the drone's current location
     *
     * @param turnsNb   the number of turns for which the drone needs to wait
     * @param turnsLeft the number of turns left
     * @return String the value to wait
     */
    public String wait(int turnsNb, int turnsLeft) throws OutOfTimeException {
        if (turnsNb > turnsLeft) {
            throw new OutOfTimeException(turnsNb, turnsLeft);
        } else {
            available = turnsNb;
            currentAction = "Waiting for ";
            return id + " " + CodeAction.WAIT.get() + " " + available;
        }
    }

    /**
     * Determines if the drone is available or not. Whenever the drone
     * is waiting of flying, the drone is unavailable
     *
     * @return true if the drone is available, false otherwise
     */
    public boolean isAvailable() {
        return available == 0;
    }

    /**
     * Update the availability of the drone and build the command line description
     *
     * @return the command description
     * @throws BadWeightException      - triggered if the drone can't carry items anymore
     * @throws EmptyInventoryException - triggered either if the drone is empty, or if the warehouse is empty
     */
    public String update() throws BadWeightException, EmptyInventoryException, NotEnoughItemsException {
        String str = "";
        if (available == 1 || available == 0) {
            switch (currentAction.charAt(0)) {
                case 'L':
                    add(product, itemNb);
                    warehouse.remove(product, itemNb);
                    str = id + " " + CodeAction.LOAD.get() + " " + warehouse.getId() + " " + product.getId() + " " + itemNb;
                    break;
                case 'U':
                    remove(product, itemNb);
                    warehouse.add(product, itemNb);
                    str = id + " " + CodeAction.UNLOAD.get() + " " + warehouse.getId() + " " + product.getId() + " " + itemNb;
                    break;
                case 'D':
                    remove(product, itemNb);
                    order.removeProduct(product, itemNb);
                    order.unAffect(this);
                    str = id + " " + CodeAction.DELIVER.get() + " " + order.getId() + " " + product.getId() + " " + itemNb;
                    break;
            }
        }
        if (available > 0) {
            available--;
        }
        return str;
    }

    /**
     * Reset the data after the update
     */
    public void reset() {
        this.order = null;
        this.itemNb = 0;
        this.warehouse = null;
        this.product = null;
        this.currentAction = "Awaiting orders.";
    }

    /**
     * Add a product to carry
     *
     * @param product - the new product to carry
     * @param q       - the quantity of product to add
     * @throws BadWeightException if the drone can't carry items anymore
     */
    public void add(Product product, int q) throws BadWeightException {
        if (getWeight() > Parser.MAX_PAYLOAD) {
            throw new BadWeightException(getWeight());
        } else if (carriedProducts.containsKey(product)) {
            carriedProducts.put(product, carriedProducts.get(product) + q);
        } else carriedProducts.put(product, q);
    }

    /**
     * Remove the product from the drone's inventory
     *
     * @param product - the product to remove from the drone's inventory
     * @param q       - the quantity of product to add
     * @throws EmptyInventoryException triggered if the drone can't carry more items
     * @throws NotEnoughItemsException - if failing to remove from an inventory
     */
    public void remove(Product product, int q) throws EmptyInventoryException, NotEnoughItemsException {
        if (carriedProducts.isEmpty()) {
            throw new EmptyInventoryException();
        } else carriedProducts.removeContents(product, q);
    }

    /**
     * Calculates the weight of the list of products carried by the drone
     *
     * @return the current weight of the whole products the drone carries
     */
    public int getWeight() {
        int weight = 0;
        for (Product product : carriedProducts.keySet()) {
            weight += product.getWeight() * carriedProducts.get(product);
        }
        return weight;
    }

    @Override
    public String toString() {
        String result = "";
        if (!currentAction.startsWith("D")) {
            result = getPosition();
        }
        if (!currentAction.startsWith("A") && !currentAction.startsWith("W")) {
            result += currentAction + (currentActionTotal - available) + "/" + currentActionTotal + " turns.";
        } else if (currentAction.startsWith("W")) {
            result += getPosition() + currentAction + available + " more turns.";
        } else {
            result += currentAction;
        }
        return result + " Inventory: \n" + carriedProducts.toString().replaceAll(" to deliver", "");
    }

    public int getId() {
        return id;
    }

    public Coord getLocation() {
        return location;
    }

    public Inventory getCarriedProducts() {
        return carriedProducts;
    }

    public String getPosition() {
        return "Located at (" + location.getX() + "," + location.getY() + "). ";
    }

    public void resetString() {
        currentAction = "Awaiting orders.";
    }

}
