package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;

/**
 * @author Florian Bourniquel
 * @version 1.0
 *          <p>
 *          Represent a product
 */
public class Product {

    private int id;
    private int weight;

    /**
     * Constructs the product. Sets up its id and weight.
     *
     * @param id     - the id of the product
     * @param weight - the weight of the product
     * @throws BadWeightException - if a incorrect weight was found
     */
    public Product(int id, int weight) throws BadWeightException {
        this.id = id;
        if (weight > Parser.MAX_PAYLOAD)
            throw new BadWeightException(weight);
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return id == product.id;

    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "Product #" + id;
    }

    public int getId() {
        return id;
    }

    public int getWeight() {
        return weight;
    }

}
