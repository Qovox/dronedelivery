package fr.unice.polytech.si3.drone.exception;

/**
 * @author Jeremy Lara
 * @version 1.0
 *
 * Thrown when the inventory of the drone is empty and
 * the action unload or deliver is called on this drone
 */
public class EmptyInventoryException extends Exception {

    public EmptyInventoryException() {
        super("Incorrect command, tried to remove an item from an empty inventory");
        System.out.println(this.getMessage());
        System.exit(3);
    }

}
