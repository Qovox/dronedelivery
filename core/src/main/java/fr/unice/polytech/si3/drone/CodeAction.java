package fr.unice.polytech.si3.drone;

/**
 * Enum for the action codes
 */
public enum CodeAction {

    LOAD('L'),
    UNLOAD('U'),
    WAIT('W'),
    DELIVER('D');

    private char val;

    CodeAction(char val) {
        this.val = val;
    }

    public char get() {
        return this.val;
    }

}
