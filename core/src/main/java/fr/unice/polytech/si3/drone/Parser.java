package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.SyntaxErrorException;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Guillaume Andre
 * @version 1.0
 *          <p>
 *          Parses the input file and initializes the drones, warehouses and orders to
 *          carry out later.
 */
public class Parser {

    public static int MAX_PAYLOAD;
    public static int MAX_TURNS;
    public static Coord DIMENSIONS;

    private BufferedReader br;
    private Pattern number;
    private int droneCount;
    private List<Product> products;
    private List<Drone> drones;
    private List<Warehouse> warehouses;
    private List<Order> orders;

    /**
     * Standard constructor.
     *
     * @param input - the file containing the initialization data
     * @throws FileNotFoundException - if the file was not found
     */
    public Parser(File input) throws FileNotFoundException {
        br = new BufferedReader(new FileReader(input));
        number = Pattern.compile("[0-9]+");
        products = new ArrayList<>();
        drones = new ArrayList<>();
        warehouses = new ArrayList<>();
        orders = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return products;
    }

    public List<Drone> getDrones() {
        return drones;
    }

    public List<Warehouse> getWarehouses() {
        return warehouses;
    }

    public List<Order> getOrders() {
        return orders;
    }

    /**
     * Parses the input file and create the drones, warehouses and orders.
     *
     * @throws IOException          - if unable to read the file
     * @throws BadWeightException   - if one of the product has a weight greater than the drone max payload
     * @throws SyntaxErrorException - if the format of the input file is incorrect
     */
    public void parse() throws IOException, BadWeightException, SyntaxErrorException {
        parseInitial(br.readLine());
        parseProducts();
        parseWarehouses();
        parseOrders();
        setupDrones();
    }

    /**
     * Parses initialization data.
     *
     * @param line - the first line of the input
     * @throws SyntaxErrorException - if the format of the input file is incorrect
     */
    private void parseInitial(String line) throws SyntaxErrorException {
        try {
            Matcher matcher = number.matcher(line);
            matcher.find();
            int rows = Integer.parseInt(matcher.group());
            matcher.find();
            int columns = Integer.parseInt(matcher.group());
            DIMENSIONS = new Coord(rows, columns);
            matcher.find();
            droneCount = Integer.parseInt(matcher.group());
            matcher.find();
            MAX_TURNS = Integer.parseInt(matcher.group());
            matcher.find();
            MAX_PAYLOAD = Integer.parseInt(matcher.group());
            if (matcher.find()) {
                System.out.println("Warning: extra data in the first line of context.");
            }
        } catch (Exception e) {
            throw new SyntaxErrorException("Incorrect format in the first line of input.");
        }
    }

    /**
     * Parses the available products for delivery.
     *
     * @throws IOException          - if failing to read the file
     * @throws BadWeightException   - if one of the product has a weight greater than the drone max payload
     * @throws SyntaxErrorException - if the format of the input file is incorrect
     */
    private void parseProducts() throws IOException, BadWeightException, SyntaxErrorException {
        try {
            Matcher matcher = number.matcher(br.readLine());
            matcher.find();
            int productCount = Integer.parseInt(matcher.group());
            matcher = number.matcher(br.readLine());
            for (int i = 0; i < productCount; i++) {
                matcher.find();
                products.add(new Product(i, Integer.parseInt(matcher.group())));
            }
            if (matcher.find()) {
                System.out.println("Warning: extra data for products.");
            }
        } catch (Exception e) {
            throw new SyntaxErrorException("Incorrect format for products.");
        }
    }

    /**
     * Parses the data for all warehouses.
     *
     * @throws SyntaxErrorException - if the format of the input file is incorrect
     * @throws IOException          - if failing to read the file
     */
    private void parseWarehouses() throws IOException, SyntaxErrorException {
        try {
            Matcher matcher = number.matcher(br.readLine());
            matcher.find();
            int warehouseCount = Integer.parseInt(matcher.group());
            int x, y;
            for (int i = 0; i < warehouseCount; i++) {
                matcher = number.matcher(br.readLine());
                matcher.find();
                x = Integer.parseInt(matcher.group());
                matcher.find();
                y = Integer.parseInt(matcher.group());
                Map<Product, Integer> contents = new HashMap<>();
                matcher = number.matcher(br.readLine());
                for (Product product : products) {
                    matcher.find();
                    contents.put(product, Integer.parseInt(matcher.group()));
                }
                if (matcher.find()) {
                    System.out.println("Warning: extra data for warehouse products.");
                }
                warehouses.add(new Warehouse(i, contents, new Coord(x, y)));
            }
        } catch (Exception e) {
            throw new SyntaxErrorException("Incorrect format for warehouses.");
        }
    }

    /**
     * Parses the data of the orders to fulfill.
     *
     * @throws IOException          - if failing to read the file
     * @throws SyntaxErrorException - if the format of the input file is incorrect
     */
    private void parseOrders() throws IOException, SyntaxErrorException {
        try {
            Matcher matcher = number.matcher(br.readLine());
            matcher.find();
            int orderCount = Integer.parseInt(matcher.group());
            int x, y;
            for (int i = 0; i < orderCount; i++) {
                matcher = number.matcher(br.readLine());
                matcher.find();
                x = Integer.parseInt(matcher.group());
                matcher.find();
                y = Integer.parseInt(matcher.group());
                matcher = number.matcher(br.readLine());
                matcher.find();
                int productCount = Integer.parseInt(matcher.group());
                matcher = number.matcher(br.readLine());
                Map<Product, Integer> contents = new HashMap<>();
                for (int j = 0; j < productCount; j++) {
                    matcher.find();
                    int productId = Integer.parseInt(matcher.group());
                    Product product = products.get(productId);
                    if (contents.get(product) == null) {
                        contents.put(product, 1);
                    } else {
                        contents.put(product, contents.get(product) + 1);
                    }
                }
                if (matcher.find()) {
                    System.out.println("Warning : extra products in order, will not be considered.");
                }
                orders.add(new Order(contents, new Coord(x, y), i));
            }
        } catch (Exception e) {
            throw new SyntaxErrorException("Incorrect format for orders.");
        }
    }

    /**
     * Creates the drones. Drones start in the first warehouse.
     */
    private void setupDrones() {
        for (int i = 0; i < droneCount; i++) {
            drones.add(new Drone(i, new HashMap<>(), warehouses.get(0).getLocation()));
        }
    }

}
