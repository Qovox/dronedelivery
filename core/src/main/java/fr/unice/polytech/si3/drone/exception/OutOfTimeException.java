package fr.unice.polytech.si3.drone.exception;

/**
 * @author Jeremy Lara
 * @version 1.0
 *
 * Thrown when the drone has not enough time to perform the required action
 */
public class OutOfTimeException extends Exception {

    public OutOfTimeException(int turnsNb, int turnsLeft) {
        super("Not enough time to perform this action, requested time : " + turnsNb + " ; time left : " + turnsLeft);
    }

}
