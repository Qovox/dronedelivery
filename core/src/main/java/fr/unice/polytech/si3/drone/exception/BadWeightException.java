package fr.unice.polytech.si3.drone.exception;

import fr.unice.polytech.si3.drone.Parser;

/**
 * @author Florian Bourniquel
 * @version 1.0
 *
 * Thrown when a product is created with a incorrect weight
 * or when the drone is overloaded
 */
public class BadWeightException extends Exception {

    public BadWeightException(int weight) {
        super("Incorrect weight, max value : " + Parser.MAX_PAYLOAD + " actual : " + weight);
        System.out.println(this.getMessage());
        System.exit(1);
    }
}
