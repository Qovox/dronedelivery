package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Helper class to wrap map of product/integer
 */
public class Inventory {

    private Map<Product, Integer> products;

    /**
     * Standard constructor.
     *
     * @param products - the map of the products and their quantity in this inventory
     */
    public Inventory(Map<Product, Integer> products) {
        this.products = new HashMap<>(products);
    }

    /**
     * Returns the size of this inventory.
     *
     * @return the inventory size.
     */
    public int size() {
        return products.size();
    }

    /**
     * Returns the amount of a product this inventory contains, or
     * null if no product of this type is available
     *
     * @param p - the product to search
     */
    public Integer get(Product p) {
        return products.get(p);
    }

    /**
     * Checks if a product is present in this inventory or not.
     *
     * @param p - the product to search
     * @return true if found, false otherwise
     */
    public boolean containsKey(Product p) {
        return products.containsKey(p);
    }

    /**
     * Adds a given quantity of a product in this inventory. Erases the previous
     * quantity of the product.
     *
     * @param p - the product to add
     * @param q - the amount to put
     */
    public void put(Product p, Integer q) {
        products.put(p, q);
    }

    /**
     * Returns a set of all products available in this inventory.
     */
    public Set<Product> keySet() {
        return products.keySet();
    }

    /**
     * Checks if this inventory is empty or not.
     *
     * @return true if no products are contained within
     */
    public boolean isEmpty() {
        return products.size() == 0;
    }

    /**
     * Removes a given amount of a given product from this inventory.
     *
     * @param p - the product type to remove
     * @param q - the amount to remove
     * @throws NotEnoughItemsException - if failing to remove
     */
    public void removeContents(Product p, int q) throws NotEnoughItemsException {
        if (!products.containsKey(p))
            throw new NotEnoughItemsException();
        int realQuantity = products.get(p);
        if (realQuantity < q)
            throw new NotEnoughItemsException();
        else if (realQuantity == q) {
            products.remove(p);
        } else {
            products.put(p, realQuantity - q);
        }
    }

    @Override
    public String toString() {
        String result = "";
        if (products.isEmpty()) {
            return "\tEmpty.\n";
        }
        for (Product product : products.keySet()) {
            if (products.get(product) > 0) {
                result += "\t" + product.toString() + " : " + products.get(product) + " left to deliver.\n";
            }
        }
        return result;
    }

}
