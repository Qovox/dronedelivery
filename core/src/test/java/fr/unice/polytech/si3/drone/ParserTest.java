package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.SyntaxErrorException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * @author Guillaume Andre
 *         <p>
 *         Test class for Parser.
 */
public class ParserTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public ExpectedSystemExit exit = ExpectedSystemExit.none();

    private Parser parser;

    public static Parser setUp(String content, File file) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(content);
        bw.close();
        return new Parser(file);
    }

    @Test
    public void initTest() throws IOException, BadWeightException, SyntaxErrorException {
        String content = "7 5 2 25 500 \n" +
                "3 \n" +
                "100 5 450 \n" +
                "1 \n" +
                "0 0 \n" +
                "5 1 0 \n" +
                "1 \n" +
                "1 1 \n" +
                "2 \n" +
                "2 0";
        parser = setUp(content, folder.newFile());
        parser.parse();
        assertEquals(7, Parser.DIMENSIONS.getX());
        assertEquals(5, Parser.DIMENSIONS.getY());
        assertEquals(2, parser.getDrones().size());
        assertEquals(25, Parser.MAX_TURNS);
        assertEquals(500, Parser.MAX_PAYLOAD);
        assertEquals(1, parser.getWarehouses().size());
        assertEquals(0, parser.getWarehouses().get(0).getLocation().getX());
        assertEquals(0, parser.getWarehouses().get(0).getLocation().getY());
        assertEquals(5, (int) parser.getWarehouses().get(0).getProducts().get(parser.getProducts().get(0)));
        assertEquals(1, (int) parser.getWarehouses().get(0).getProducts().get(parser.getProducts().get(1)));
        assertEquals(0, (int) parser.getWarehouses().get(0).getProducts().get(parser.getProducts().get(2)));
        assertEquals(1, parser.getOrders().size());
        assertEquals(1, parser.getOrders().get(0).getDestination().getX());
        assertEquals(1, parser.getOrders().get(0).getDestination().getY());
        assertEquals(1, (int) parser.getOrders().get(0).getContents().get(parser.getProducts().get(2)));
        assertEquals(1, (int) parser.getOrders().get(0).getContents().get(parser.getProducts().get(0)));
    }

    @Test
    public void missingInitLineTest() throws SyntaxErrorException, IOException, BadWeightException {
        String content = "3 \n" +
                "100 5 450 \n" +
                "1 \n" +
                "0 0 \n" +
                "5 1 0 \n" +
                "1 \n" +
                "1 1 \n" +
                "2 \n" +
                "2 0";
        parser = setUp(content, folder.newFile());
        exit.expectSystemExitWithStatus(2);
        parser.parse();
    }

    @Test
    public void wrongProducts() throws IOException, BadWeightException, SyntaxErrorException {
        String content = "7 5 2 25 500 \n" +
                "3 \n" +
                "100 5 \n" +
                "1 \n" +
                "0 0 \n" +
                "5 1 0 \n" +
                "1 \n" +
                "1 1 \n" +
                "2 \n" +
                "2 0";
        parser = setUp(content, folder.newFile());
        exit.expectSystemExitWithStatus(2);
        parser.parse();
    }

    @Test
    public void wrongWarehouse() throws IOException, BadWeightException, SyntaxErrorException {
        String content = "7 5 2 25 500 \n" +
                "3 \n" +
                "100 5 450 \n" +
                "3 \n" +
                "0 0 \n" +
                "5 1 0 \n" +
                "1 \n" +
                "1 1 \n" +
                "2 \n" +
                "2 0";
        parser = setUp(content, folder.newFile());
        exit.expectSystemExitWithStatus(2);
        parser.parse();
    }

    @Test
    public void wrongOrder() throws IOException, BadWeightException, SyntaxErrorException {
        String content = "7 5 2 25 500 \n" +
                "3 \n" +
                "100 5 450 \n" +
                "1 \n" +
                "0 0 \n" +
                "5 1 0 \n" +
                "3 \n" +
                "1 1 \n" +
                "2 \n" +
                "2 0";
        parser = setUp(content, folder.newFile());
        exit.expectSystemExitWithStatus(2);
        parser.parse();
    }

    @Test
    public void wrongOrder2() throws IOException, BadWeightException, SyntaxErrorException {
        String content = "7 5 2 25 500 \n" +
                "3 \n" +
                "100 5 450 \n" +
                "1 \n" +
                "0 0 \n" +
                "5 1 0 \n" +
                "1 \n" +
                "1 1 \n" +
                "3 \n" +
                "2 0";
        parser = setUp(content, folder.newFile());
        exit.expectSystemExitWithStatus(2);
        parser.parse();
    }

}
