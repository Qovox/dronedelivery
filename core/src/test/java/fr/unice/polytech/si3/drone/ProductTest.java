package fr.unice.polytech.si3.drone;


import org.junit.*;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

/**
 * @author Florian Bourniquel
 *
 * Test class for Product.
 */
public class ProductTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Test
    public void badWeightException() throws Exception {
        Parser.MAX_PAYLOAD = 10;
        exit.expectSystemExitWithStatus(1);
        new Product (5,50);

    }

}