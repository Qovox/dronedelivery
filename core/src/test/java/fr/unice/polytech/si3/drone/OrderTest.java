package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Guillaume Andre
 *         <p>
 *         Test class for Order. Most of the functionality of the
 *         class is actually already tested in DroneTest.
 */
public class OrderTest {

    private Order order;

    public static Map<Product, Integer> createTestMap() throws BadWeightException {
        Map<Product, Integer> content = new HashMap<>();
        content.put(new Product(0, 0), 5);
        content.put(new Product(1, 0), 2);
        return content;
    }

    @Test
    public void stringTest() throws BadWeightException {
        order = new Order(createTestMap(), new Coord(), 1);
        String description = order.toString();
        assert description.contains("(0,0)");
        assert description.contains("Product #0");
        assert description.contains("Product #1");
        assert description.contains("Scheduled");
        assert description.contains("None");
    }

}
