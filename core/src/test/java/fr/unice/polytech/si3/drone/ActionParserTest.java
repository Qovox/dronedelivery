package fr.unice.polytech.si3.drone;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Guillaume Andre
 *         <p>
 *         Test class for Action Parser.
 */
public class ActionParserTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private ActionParser ap;

    public void setUp(String content) throws IOException {
        File file = folder.newFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(content);
        bw.close();
        ap = new ActionParser(file);
    }

    @Test
    public void parseTest() throws IOException {
        String content = " 4 \n 0 W 4 \n 1 L 0 1 1 \n 1 D 1 0 3 \n 2 U 0 0 0";
        setUp(content);
        List<Drone> drones = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            drones.add(new Drone(i, new HashMap<>(), new Coord()));
        }
        List<ActionQueue> actions = ap.parse(drones);
        assert !actions.get(0).hasAction();
        assertEquals(" W 4 ", actions.get(0).getActions().get(0));
        assertEquals(" L 0 1 1 ", actions.get(1).getActions().get(0));
        assertEquals(" D 1 0 3 ", actions.get(1).getActions().get(1));
        assertEquals(" U 0 0 0", actions.get(2).getActions().get(0));
    }

}
