package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;
import org.junit.Test;

/**
 * @author Guillaume Andre
 *         <p>
 *         Test class for warehouse. Most of its functionality is already
 *         tested in DroneTest.
 */
public class WarehouseTest {

    private Warehouse warehouse;

    @Test
    public void stringTest() throws BadWeightException {
        warehouse = new Warehouse(0, OrderTest.createTestMap(), new Coord(3,4));
        String description = warehouse.toString();
        assert description.contains("Warehouse #0");
        assert description.contains("(3,4)");
        assert description.contains("Product #0");
        assert description.contains("Product #1");
    }


}
