package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;
import org.junit.*;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author Jeremy Lara
 *         <p>
 *         Test class for the drone
 */
public class DroneTest {

    @Rule
    public ExpectedSystemExit exit = ExpectedSystemExit.none();
    private Drone drone;
    private Map<Product, Integer> productsQuantity;
    private Warehouse warehouse;
    private Product p1;
    private Product p2;

    @Before
    public void setUp() throws BadWeightException {
        Parser.MAX_PAYLOAD = 500;
        Parser.MAX_TURNS = 50;

        p1 = new Product(0, 25);
        p2 = new Product(1, 50);

        productsQuantity = new HashMap<>();
        productsQuantity.put(p1, 1);
        productsQuantity.put(p2, 1);
        productsQuantity.put(new Product(2, 100), 1);
        productsQuantity.put(new Product(3, 200), 1);
        productsQuantity.put(new Product(4, 400), 1);
    }

    @After
    public void tearDown() {
        Parser.MAX_PAYLOAD = 0;
        Parser.MAX_TURNS = 0;
    }

    @Test
    public void droneEmpty() throws BadWeightException {
        drone = new Drone(0, new HashMap<>(), new Coord(0, 2));
        assertEquals(0, drone.getId());
        assertEquals(0, drone.getCarriedProducts().size());
        assertEquals(0, drone.getLocation().getX());
        assertEquals(2, drone.getLocation().getY());
    }

    @Test
    public void load() throws BadWeightException, EmptyInventoryException, OutOfTimeException, NotEnoughItemsException {
        warehouse = new Warehouse(0, productsQuantity, new Coord(3, 2));
        drone = new Drone(0, new HashMap<>(), new Coord(0, 0));
        Product product = new Product(0, 25);
        drone.load(warehouse, product, 1, 25);
        for (int i = 0; i < 3; i++) {
            drone.update();
        }
        assertEquals("0 L 0 0 1", drone.update());
        assertEquals(1, drone.getCarriedProducts().size());
        assertEquals(25, drone.getWeight());
        product = new Product(4, 400);
        drone.load(warehouse, product, 1, 25);
        assertEquals("0 L 0 4 1", drone.update());
        assertEquals(2, drone.getCarriedProducts().size());
        assertEquals(425, drone.getWeight());
    }

    @Test
    public void unload() throws EmptyInventoryException, BadWeightException, OutOfTimeException, NotEnoughItemsException {
        warehouse = new Warehouse(0, productsQuantity, new Coord(1, 4));
        drone = new Drone(0, new HashMap<>(), new Coord(0, 0));
        drone.add(new Product(3, 200), 1);
        drone.add(new Product(1, 50), 1);
        assertEquals(250, drone.getWeight());
        assertEquals(2, drone.getCarriedProducts().size());
        drone.unload(warehouse, new Product(1, 50), 1, 25);
        for (int i = 0; i < 4; i++) {
            drone.update();
        }
        assertEquals("0 U 0 1 1", drone.update());
        assertEquals(1, drone.getCarriedProducts().size());
        assertEquals(200, drone.getWeight());
        drone.unload(warehouse, new Product(3, 200), 1, 25);
        assertEquals("0 U 0 3 1", drone.update());
        assertEquals(0, drone.getCarriedProducts().size());
        assertEquals(0, drone.getWeight());
        drone.add(new Product(5, 250), 1);
        drone.unload(warehouse, new Product(5, 250), 1, 25);
        assertEquals("0 U 0 5 1", drone.update());
        assertEquals(6, warehouse.getProducts().size());
    }

    @Test
    public void unloadException() throws BadWeightException, EmptyInventoryException, NotEnoughItemsException {
        warehouse = new Warehouse(0, productsQuantity, new Coord(1, 4));
        drone = new Drone(0, new HashMap<>(), new Coord(0, 0));
        exit.expectSystemExitWithStatus(3);
        drone.remove(new Product(0, 50), 1);
    }

    @Test
    public void deliver() throws EmptyInventoryException, BadWeightException, OutOfTimeException, NotEnoughItemsException {
        Map<Product, Integer> orderedProd = new HashMap<>();
        orderedProd.put(new Product(2, 100), 3);
        orderedProd.put(new Product(3, 200), 1);
        Order order = new Order(orderedProd, new Coord(3, 2), 3);
        drone = new Drone(1, new HashMap<>(), new Coord(0, 0));
        drone.add(new Product(2, 100), 3);
        drone.add(new Product(3, 200), 1);
        assertEquals(500, drone.getWeight());
        drone.deliver(order, new Product(2, 100), 3, 25);
        for (int i = 0; i < 3; i++) {
            drone.update();
        }
        assertEquals("1 D 3 2 3", drone.update());
        assertEquals(1, drone.getCarriedProducts().size());
        assertEquals(200, drone.getWeight());
        drone.deliver(order, new Product(3, 200), 1, 25);
        assertEquals("1 D 3 3 1", drone.update());
        assertEquals(0, drone.getCarriedProducts().size());
        assertEquals(0, drone.getWeight());
    }

    @Test
    public void waitDrone() throws OutOfTimeException {
        drone = new Drone(0, new HashMap<>(), new Coord(0, 0));
        //assertEquals("0 W 15", drone.wait(15, 25));
    }

    @Test
    public void cantWait() throws OutOfTimeException {
        drone = new Drone(0, new HashMap<>(), new Coord(1, 3));
        drone.wait(75, 75);
    }

    @Test
    public void weight() throws BadWeightException {
        drone = new Drone(1, new HashMap<>(), new Coord(1, 2));
        drone.add(new Product(1, 75), 1);
        assertEquals(75, drone.getWeight());
    }

    @Test
    public void add() throws BadWeightException {
        drone = new Drone(2, new HashMap<>(), new Coord(2, 3));
        drone.add(new Product(2, 125), 1);
        drone.add(new Product(4, 50), 1);
        assertEquals(2, drone.getCarriedProducts().size());
    }

    @Test
    public void addWithException() throws BadWeightException {
        Parser.MAX_PAYLOAD = 50;
        exit.expectSystemExitWithStatus(1);
        drone.add(new Product(2, 125), 1);
    }

    @Test
    public void remove() throws EmptyInventoryException, NotEnoughItemsException {
        drone = new Drone(2, productsQuantity, new Coord(1, 4));
        drone.remove(p1, 1);
        drone.remove(p2, 1);
        assertEquals(3, drone.getCarriedProducts().size());
    }

    @Test
    public void removeWithException() throws EmptyInventoryException, BadWeightException, NotEnoughItemsException {
        drone = new Drone(2, new HashMap<>(), new Coord(0, 1));
        exit.expectSystemExitWithStatus(3);
        drone.remove(new Product(3, 150), 1);
    }

    @Test
    public void available() throws BadWeightException, OutOfTimeException, EmptyInventoryException, NotEnoughItemsException {
        warehouse = new Warehouse(0, productsQuantity, new Coord(1, 4));
        drone = new Drone(0, new HashMap<>(), new Coord(0, 0));
        drone.load(warehouse, new Product(1, 50), 1, 25);
        assertFalse(drone.isAvailable());
        for (int i = 0; i < 5; i++) {
            drone.update();
        }
        assertTrue(drone.isAvailable());
    }

    @Test
    public void toStringTest() {
        drone = new Drone(0, productsQuantity, new Coord(0, 0));
        String description = drone.toString();
        System.out.println(description);
        assert description.contains("(0,0)");
        assert description.contains("Awaiting orders");
        for (int i = 0; i <= 4; i++) {
            assert description.contains("Product #" + i);
        }
    }

}
