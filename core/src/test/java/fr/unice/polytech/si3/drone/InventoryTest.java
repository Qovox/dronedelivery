package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;
import org.junit.*;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * Test for the inventory class
 */
public class InventoryTest {

    @Test
    public void testRemove() throws BadWeightException, NotEnoughItemsException {
        Parser.MAX_PAYLOAD = 100;
        Inventory i = new Inventory(new HashMap<>());
        Product p1 = new Product(1, 10);
        Product p2 = new Product(2, 10);
        i.put(p1, 1);
        i.put(p2, 20);
        i.removeContents(p1, 1);
        i.removeContents(p2, 5);
        assertEquals(1, i.keySet().size());
        assertEquals((Integer) 15, i.get(p2));
        i.removeContents(p2, 15);
        assertEquals(0, i.keySet().size());
    }

    @Before
    public void reset() {
        Parser.MAX_PAYLOAD = 0;
    }

}
