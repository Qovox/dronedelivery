package fr.unice.polytech.si3.drone;

import fr.unice.polytech.si3.drone.exception.BadWeightException;
import fr.unice.polytech.si3.drone.exception.EmptyInventoryException;
import fr.unice.polytech.si3.drone.exception.NotEnoughItemsException;
import fr.unice.polytech.si3.drone.exception.OutOfTimeException;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * @author Guillaume Andre
 *         <p>
 *         Test class for actionQueue.
 */
public class ActionQueueTest {

    private ActionQueue queue;

    @Test
    public void actionsTest() throws BadWeightException, OutOfTimeException, EmptyInventoryException, NotEnoughItemsException {
        Drone drone = new Drone(0, new HashMap<>(), new Coord());
        queue = new ActionQueue(drone);
        queue.addAction("L 0 0 1");
        queue.addAction("W 1");
        queue.addAction("D 0 0 1");
        Product product = new Product(0, 0);
        Map<Product, Integer> wareContents = new HashMap<>();
        wareContents.put(product, 1);
        List<Warehouse> warehouses = Collections.singletonList(new Warehouse(0, wareContents, new Coord()));
        List<Order> orders = Collections.singletonList(new Order(wareContents, new Coord(0, 4), 0));
        List<Product> products = Collections.singletonList(product);
        queue.doAction(warehouses, products, orders, 25); //load
        assert drone.isAvailable();
        assert drone.getCarriedProducts().containsKey(product);
        queue.doAction(warehouses, products, orders, 24); //wait
        assert drone.isAvailable();
        queue.doAction(warehouses, products, orders, 23); //deliver
        assert !drone.isAvailable();
        queue.doAction(warehouses, products, orders, 22);
        assert orders.get(0).getContents().size() == 1;
    }


    @Test
    public void loadUnloadTest() throws BadWeightException, OutOfTimeException, EmptyInventoryException, NotEnoughItemsException {
        Drone drone = new Drone(0, new HashMap<>(), new Coord());
        queue = new ActionQueue(drone);
        queue.addAction("L 0 0 1");
        queue.addAction("U 0 0 1");
        Product product = new Product(0, 0);
        Map<Product, Integer> wareContents = new HashMap<>();
        wareContents.put(product, 1);
        List<Warehouse> warehouses = Collections.singletonList(new Warehouse(0, wareContents, new Coord()));
        List<Order> orders = new ArrayList<>();
        List<Product> products = Collections.singletonList(product);
        queue.doAction(warehouses, products, orders, 25); //load
        assert drone.isAvailable();
        assert drone.getCarriedProducts().containsKey(product);
        queue.doAction(warehouses, products, orders, 24); //unload
        assert drone.isAvailable();
        assert drone.getCarriedProducts().isEmpty();
        assertEquals(1, (int) wareContents.get(product));
    }

}
