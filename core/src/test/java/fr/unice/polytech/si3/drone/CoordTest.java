package fr.unice.polytech.si3.drone;

import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 * Tests for the customer class
 */
public class CoordTest {

    @Test
    public void testDistance1() {
        Coord c1 = new Coord();
        Coord c2 = new Coord();
        assertEquals(0, c1.distance(c2));
    }

    @Test
    public void testDistance2() {
        Coord c1 = new Coord();
        Coord c2 = new Coord(0, 2);
        assertEquals(2, c1.distance(c2));
    }

    @Test
    public void testDistance3() {
        Coord c1 = new Coord(4, 5);
        Coord c2 = new Coord(2, 6);
        assertEquals(3, c1.distance(c2));
    }

    @Test
    public void testDistance4() {
        Coord c1 = new Coord(25, 34);
        Coord c2 = new Coord(86, 13);
        assertEquals(65, c1.distance(c2));
    }
}
